# -*- coding: utf-8 -*-
"""
    enDI specific exception
"""


class BadRequest(Exception):
    """
    Exception raised when the request is invalid (form invalid datas ...)
    """
    message = u"La requête est incorrecte"

    def messages(self):
        """
        Used to fit colander's Invalid exception api
        """
        return [self.message]

    def asdict(self, translate=None):
        return {'erreur': self.message}


class Forbidden(Exception):
    """
        Forbidden exception, used to raise a forbidden action error
    """
    message = u"Vous n'êtes pas autorisé à effectuer cette action"


class SignatureError(Forbidden):
    """
        Exception for status modification calls with the wrong signature
    """
    message = u"Des informations manquent pour effectuer cette action"
