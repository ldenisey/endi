# -*- coding: utf-8 -*-
from colanderalchemy import SQLAlchemySchemaNode

from endi.models.files import FileType
from endi.forms import customize_field


def get_admin_file_type_schema():
    """
    Build a schema to administrate file types

    :rtype: :class:`colanderalchemy.SQLAlchemySchemaNode`
    """
    schema = SQLAlchemySchemaNode(FileType, includes=('label',))
    customize_field(schema, 'label', title=u"Libellé")
    return schema
