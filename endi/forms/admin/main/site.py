# -*- coding: utf-8 -*-
import colander
from endi.forms import files
from endi import forms


class SiteConfigSchema(colander.MappingSchema):
    """
        Site configuration
        logos ...
    """
    logo = files.ImageNode(
        title=u"Choisir un logo",
        missing=colander.drop,
        description=u"Charger un fichier de type image *.png *.jpeg \
 *.jpg…")

    welcome = forms.textarea_node(
        title=u"Texte d'accueil",
        richwidget=True,
        missing=u'',
        admin=True,
    )
