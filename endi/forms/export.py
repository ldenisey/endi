# -*- coding: utf-8 -*-
"""
    Form schemas for invoice exports
"""
import colander
import deform

from endi.models.expense.sheet import get_expense_years
from endi.forms.user import contractor_filter_node_factory
from endi.forms.company import company_filter_node_factory
from endi.forms.tasks.invoice import InvoicesRangeSchema
from endi import forms


class ExportedField(colander.SchemaNode):
    schema_type = colander.Boolean
    title = u""
    label = u"Inclure les éléments déjà exportés ?"
    description = (
        u"enDI retient les éléments qui ont déjà été "
        u"exportés, vous pouvez décider ici de les inclure"
    )
    default = False
    missing = False


class PeriodSchema(colander.MappingSchema):
    """
        A form used to select a period
    """
    start_date = colander.SchemaNode(colander.Date(), title=u"Date de début")
    end_date = colander.SchemaNode(
        colander.Date(),
        title=u"Date de fin",
        missing=forms.deferred_today,
        default=forms.deferred_today
    )
    exported = ExportedField()

    def validator(self, form, value):
        """
            Validate the period
        """
        if value['start_date'] > value['end_date']:
            exc = colander.Invalid(
                form,
                u"La date de début doit précéder la date de fin"
            )
            exc['start_date'] = u"Doit précéder la date de fin"
            raise exc


class InvoiceNumberSchema(InvoicesRangeSchema):
    """ Extends the date+number selector

    With filter on accountancy export status.
    """
    exported = ExportedField()


class AllSchema(colander.MappingSchema):
    pass


@colander.deferred
def deferred_category(node, kw):
    return kw.get('prefix', '0')


class Category(colander.SchemaNode):
    schema_type = colander.String
    widget = deform.widget.HiddenWidget()
    default = deferred_category


class ExpenseAllSchema(AllSchema):
    category = Category()


class ExpenseSchema(colander.MappingSchema):
    """
    Schema for sage expense export
    """
    user_id = contractor_filter_node_factory()
    year = forms.year_select_node(title=u"Année", query_func=get_expense_years)
    month = forms.month_select_node(title=u"Mois")
    exported = ExportedField()
    category = Category()


class ExpenseIdSchema(colander.MappingSchema):
    sheet_id = colander.SchemaNode(
        colander.Integer(),
        title=u"Identifiant",
        description=u"Identifiant de la feuille de notes de dépense "
        u"(voir sur la page associée)")
    exported = ExportedField()
    category = Category()


class SupplierInvoiceAllSchema(AllSchema):
    category = Category()


class SupplierInvoiceSchema(colander.MappingSchema):
    """
    Schema for sage supplier invoice export
    """
    company_id = company_filter_node_factory()
    exported = ExportedField()


class SupplierInvoiceIdSchema(colander.MappingSchema):
    supplier_invoice_id = colander.SchemaNode(
        colander.Integer(),
        title=u"Identifiant",
        description=u"Identifiant de la facture fournisseur "
        u"(voir sur la page associée)")
    exported = ExportedField()


class BPFYearSchema(colander.MappingSchema):
    """
    Schema for BPF export (agregate of BusinessBPFData)
    """
    year = forms.year_select_node(
        title=u"Année",
        query_func=get_expense_years,
    )
    ignore_missing_data = colander.SchemaNode(
        colander.Boolean(),
        title=u"Forcer l'export",
        description=u"Ignorer les éléments dont le BPF n'est pas rempli.",
        default=False,
        missing=False,
    )
