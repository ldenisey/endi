# -*- coding: utf-8 -*-
import colander
import deform

from endi.models.payments import (
    PaymentMode,
    BankAccount,
    Bank,
)

from endi import forms


def get_amount_topay(kw):
    """
    Retrieve the amount to be paid regarding the context
    """
    topay = 0
    context = kw['request'].context
    if getattr(context, 'type_', None) in ('invoice', 'expensesheet'):
        topay = context.topay()
    else:
        if hasattr(context, 'parent'):
            document = context.parent
            if hasattr(document, 'topay'):
                topay = document.topay()
                if hasattr(context, 'get_amount'):
                    topay += context.get_amount()
    return topay


@colander.deferred
def deferred_amount_default(node, kw):
    """
        default value for the payment amount
    """
    return get_amount_topay(kw)


@colander.deferred
def deferred_payment_mode_widget(node, kw):
    """
        dynamically retrieves the payment modes
    """
    modes = [(mode.label, mode.label) for mode in PaymentMode.query()]
    return deform.widget.SelectWidget(values=modes)


@colander.deferred
def deferred_payment_mode_validator(node, kw):
    return colander.OneOf([mode.label for mode in PaymentMode.query()])


@colander.deferred
def deferred_bank_account_widget(node, kw):
    """
    Renvoie le widget pour la sélection d'un compte bancaire
    """
    options = [(bank.id, bank.label) for bank in BankAccount.query()]
    widget = forms.get_select(options)
    return widget


@colander.deferred
def deferred_bank_account_validator(node, kw):
    return colander.OneOf([bank.id for bank in BankAccount.query()])


@colander.deferred
def deferred_customer_bank_widget(node, kw):
    """
    Renvoie le widget pour la sélection d'une banque client
    """
    options = [(bank.id, bank.label) for bank in Bank.query()]
    options.insert(0, ("", ""))
    widget = forms.get_select(options)
    return widget
