"""5.1 Add 'issuer' field on invoice payments

Revision ID: 69e690478b6c
Revises: 3024401478db
Create Date: 2019-09-26 15:40:36.901233

"""

# revision identifiers, used by Alembic.
revision = '69e690478b6c'
down_revision = '3024401478db'

from alembic import op
import sqlalchemy as sa
from endi_base.models.base import DBSESSION
from zope.sqlalchemy import mark_changed


def update_database_structure():
    op.add_column('payment', sa.Column('issuer', sa.String(255)))


def migrate_datas():
    session = DBSESSION()
    from endi.models.task.payment import Payment
    for payment in session.query(Payment).all():
        payment.issuer = payment.task.customer.label
    mark_changed(session)
    session.flush()


def upgrade():
    update_database_structure()
    migrate_datas()


def downgrade():
    op.drop_column('payment', 'issuer')
