<%inherit file="${context['main_template'].uri}" />

<%block name='content'>
<div>
	% if help_message is not None:
		<div class="alert alert-info">
			<span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#info-circle"></use></svg></span> 
			${help_message|n}
		</div>
	% endif

	<div>
		<ul class="nav nav-tabs" role="tablist">
			<% current = request.params.get('__formid__', forms.keys()[0]) %>
			% for form_name, form_datas in forms.items():
				<li role="presentation" class="${'active' if form_name==current else ''}">
					<a href="#${form_name}-container" aria-controls="${form_name}-container" role="tab" data-toggle="tab" tabindex='-1'>
						<% 
						tab_title = form_datas['title']
						tab_title = tab_title.replace("Exporter les factures fournisseurs ", "")
						tab_title = tab_title.replace("Exporter les paiements fournisseurs ", "")
						tab_title = tab_title.replace("Exporter les factures ", "")
						tab_title = tab_title.replace("Exporter les encaissements ", "")
						tab_title = tab_title.replace("Exporter les paiements ", "")
						tab_title = tab_title.replace("Exporter les ", "")
						tab_title = tab_title.replace("Exporter des ", "")
						%>
						${tab_title.capitalize()}
					</a>
				</li>
			% endfor
		</ul>
	</div>

	<div class='tab-content'>
		% for form_name, form_datas in forms.items():
			<div role="tabpanel" id="${form_name}-container" class="tab-pane fade ${'in active' if form_name==current else ''}">
				% if form_name == current and check_messages is not None:
					<div class="alert alert-info">
						<span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#info-circle"></use></svg></span> 
						${check_messages['title']}
					</div>
					% if check_messages['errors']:
						<div class="alert alert-danger">
							<p class='text-danger'>
								<span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#danger"></use></svg></span> 
							% for message in check_messages['errors']:
								<b>*</b> ${message|n}<br />
							% endfor
							</p>
						</div>
					% endif
				% endif
				${form_datas['form']|n}
			</div>
		% endfor
	</div>
	
</div>
</%block>

