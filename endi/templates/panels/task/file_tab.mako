<%def name="showfile(file_object)">
<div>
    <dl class='dl-horizontal'>
        <dt>Description du fichier</dt><dd>${file_object.description}</dd>
        <dt>Taille du fichier</dt><dd>${api.human_readable_filesize(file_object.size)}</dd>
        <dt>Dernière modification</dt><dd>${api.format_date(file_object.updated_at)}</dd>
    </dl>
    ${request.layout_manager.render_panel('menu_dropdown', label="Actions", links=stream_actions(request, file_object))}
</div>
</%def>
<div role="tabpanel" class="tab-pane row" id="attached_files">
    <div class="content_vertical_padding">
        <button class='btn btn-primary'
            onclick="window.openPopup('${add_url}')"
            title="Attacher un fichier">
            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#paperclip"></use></svg>
            Attacher un fichier
        </button>
    </div>
    <div class="content_vertical_padding">
        <h3>${title}</h3>
        % for file_object in files:
        <div class='table_container'>
            <table class='hover_table'>
            	<thead>
            		<tr>
            			<th scope="col" class="col_text">Description</th>
            			<th scope="col" class="col_number">Taille du fichier</th>
            			<th scope="col" class="col_date">Dernière modification</th>
						<th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
            		</tr>
            	</thead>
                <tbody>
                	<tr>
                		<td class="col_text">${file_object.description}</td>
                		<td class="col_number">${api.human_readable_filesize(file_object.size)}</td>
                		<td class="col_date">${api.format_date(file_object.updated_at)}</td>
                		<td class="col_actions width_one">
                			${request.layout_manager.render_panel('menu_dropdown', label="Actions", links=stream_actions(request, file_object))}
                		</td>
                	</tr>
                </tbody>
            </table>
        </div>
        % endfor
    </div>
</div>
