% if sorting_enabled:
    <%namespace file="/base/pager.mako" import="sortable"/>
% else:
    <% sortable = lambda x, y: x %>
% endif


<table class="hover_table">
    <thead>
        <tr>
			<th scope="col" class="col_status col_text"></th>
            % if is_admin_view:
                <th scope="col" class="col_text">${sortable(u"Enseigne", "company_id")}</th>
            % endif
            <th scope="col" class="col_date">${sortable(u"Date", "date")}</th>
            <th scope="col" class="col_text">${sortable(u"Nom", "name")}</th>
            % if not is_supplier_view:
                <th scope="col" class="col_text">${sortable(u"Fournisseur", "supplier")}</th>
            % endif
            <th scope="col" class="col_number">HT</th>
            <th scope="col" class="col_number">TVA</th>
            <th scope="col" class="col_number">TTC</th>
            <th scope="col" class="col_number" title="Part réglée par la CAE">${sortable(u"Part CAE", "cae_percentage")}</th>
            <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
        </tr>
    </thead>
    <tbody>
        % if records:
            % for supplier_invoice in records:
                <tr class='tableelement' id="${supplier_invoice.id}">
                    <% url = request.route_path("/suppliers_invoices/{id}", id=supplier_invoice.id) %>
                    <% onclick = "document.location='{url}'".format(url=url) %>
					<td class="col_status" onclick="${onclick}" title="${api.format_status(supplier_invoice)}">
						<span class="icon status ${supplier_invoice.status}">${api.icon(api.status_icon(supplier_invoice))}</span>
					</td>
                    % if is_admin_view:
                        <td class="col_text">
                            <a href="${request.route_path('company', id=supplier_invoice.company_id)}">${supplier_invoice.company.name}</a>
                        </td>
                    % endif
                    <td onclick="${onclick}" class="col_date">${api.format_date(supplier_invoice.date)}</td>
                    <td onclick="${onclick}" class="col_text">${supplier_invoice.name}</td>
                    % if not is_supplier_view:
                        <td onclick="${onclick}" class="col_text">${supplier_invoice.supplier_label}</td>
                    % endif
                    <td onclick="${onclick}" class="col_number">${api.format_amount(supplier_invoice.total_ht)}</td>
                    <td onclick="${onclick}" class="col_number">${api.format_amount(supplier_invoice.total_tva)}</td>
                    <td onclick="${onclick}" class="col_number">${api.format_amount(supplier_invoice.total)}</td>
                    <td onclick="${onclick}" class="col_number">${supplier_invoice.cae_percentage} %</td>
                    <td class="col_actions width_two">
                        ${request.layout_manager.render_panel('action_buttons', buttons=stream_actions(supplier_invoice))}
                    </td>
                </tr>
            % endfor
        % else:
            <tr>
                <td colspan="${'10' if is_admin_view else '9'}" class='col_text'>
                    <em>
                        % if is_search_filter_active:
                            Aucune facture fournisseur correspondant à ces critères.
                        % else:
                            Aucune facture fournisseur pour l'instant.
                        % endif
                    </em>
                </td>
            </tr>
        % endif
    </tbody>
</table>
