<%inherit file="/layouts/default.mako" />

<%block name="headtitle">
<h1>Dossier : ${layout.current_project_object.name}</h1>
<div id='popupmessage'></div>
</%block>

<%block name='actionmenucontent'>
% if request.GET.get("action") != "edit" and ( request.has_permission("edit.project", layout.current_project_object) or api.has_permission('add_phase') ):
    <div class="layout flex main_actions">
        <div role='group'>
            % if request.has_permission("edit.project", layout.current_project_object):
                <a class='btn btn-primary icon' href="${layout.edit_url}">
                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#pen"></use></svg>Modifier le dossier
                </a>
            % endif
            % if api.has_permission('add_phase'):
                <button class='btn icon' onclick="toggleModal('new_phase_form'); return false;">
                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#plus"></use></svg>Ajouter un sous-dossier
                </button>
            % endif
        </div>
    </div>
% endif
</%block>

<%block name='content'>
<div>
    <h2>${layout.current_project_object.name}</h2>
    % if layout.current_project_object.description:
        <p>${layout.current_project_object.description}</p>
    % endif
    <hr />
    <div class="layout flex two_cols">
        <div>
            % if layout.current_project_object.project_type.name != 'default':
                <p><strong>Type de dossier :</strong> ${layout.current_project_object.project_type.label}</p>
            % endif
            % if layout.current_project_object.code:
                <p><strong>Code du dossier :</strong> ${layout.current_project_object.code}</p>
            % endif
            <%block name='projecttitle'>
            <p><strong>Clients :</strong> ${', '.join(layout.customer_labels)}</p>
            </%block>
            % if layout.current_project_object.mode == 'ttc':
                <p>
                    <strong>Mode de calcul :</strong>
                    <span class="icon status mode"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#mode-ttc"></use></svg></span>
                </p>
            % endif
        </div>
        <div>
            <p><strong>Total des dépenses HT :</strong> ${api.format_amount(layout.current_project_object.get_total_expenses())}&nbsp;€</p>
            <p><strong>Total facturé HT : </strong> ${api.format_amount(layout.current_project_object.get_total_income(), precision=5)}&nbsp;€</p>
        </div>
    </div>
</div>
<div>
    <div class='tabs'>
        <%block name='rightblock'>
        ${request.layout_manager.render_panel('tabs', layout.projectmenu)}
        </%block>
    </div>
    <div class='tab-content'>
        <%block name='mainblock'>
        </%block>
    </div>
</div>
</%block>
