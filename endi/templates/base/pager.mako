<%doc>
pager template
</%doc>
<%def name="pager(items)">
<div class="pager">
  <% link_attr={"class": "btn"} %>
  <% curpage_attr={"class": "current","title": "Page en cours","aria-label": "Page en cours"} %>
  <% dotdot_attr={"class": "spacer"} %>

  ${items.pager(format="$link_previous ~2~ $link_next",
  link_attr=link_attr,
  curpage_attr=curpage_attr,
  dotdot_attr=dotdot_attr)}
</div>
</%def>
<%def name="sortable(label, column)">
<% sort_column = request.GET.get("sort", "") %>
<% sort_direction = request.GET.get("direction", "asc") %>
%if (column == sort_column):
  <% css_class = "current " + sort_direction %>
%else:
  <% css_class = ""%>
%endif
%if sort_direction == "asc":
  <% direction = "desc" %>
%else:
  <% direction = "asc"%>
%endif
<% args_dict = dict(direction=direction, sort=column) %>

<a href="${api.urlupdate(args_dict)}" class='icon ${css_class}'>
    %if column == sort_column:
        <svg class="${sort_direction}"><use href="${request.static_url('endi:static/icons/endi.svg')}#sort-${sort_direction}"></use></svg>
    %else:
        <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#sort-arrow"></use></svg>
    %endif
  ${label}
  </a>
</%def>
