<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="table_btn"/>
<%namespace file="/base/pager.mako" import="pager"/>
<%namespace file="/base/pager.mako" import="sortable"/>
<%namespace file="/base/searchformlayout.mako" import="searchform"/>

<%block name="usertitle">
    <h3>${title}</h3>
</%block>

<%block name="mainblock">

${searchform()}

<div>
    <div>
        ${records.item_count} Résultat(s)
    </div>
    <div class='table_container'>
        <table class="hover_table">
            <thead>
                <tr>
                    <th scope="col" class="col_datetime">${sortable("Date", "datetime")}</th>
                    <th scope="col" class="col_text">Intitulé de l'Atelier</th>
                    <th scope="col" class="col_text">Animateur(s)/Animatrice(s)</th>
                    <th scope="col">Nombre de participant(s)</th>
                    % if is_edit_view:
                        <th scope="col" class="col_text">Présence</th>
                    % else:
                        <th scope="col" class="col_text">Horaires</th>
                    % endif
                    <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
                </tr>
            </thead>
            <tbody>
                % for workshop in records:
                    % if request.has_permission('edit.workshop', workshop):
                        <% _query=dict(action='edit') %>
                    % else:
                        ## Route is company_workshops_subscribed, the context is the company
                        <% _query=dict() %>
                    % endif
                    <% url = request.route_path('workshop', id=workshop.id, _query=_query) %>
                    % if request.has_permission('view.workshop', workshop):
                        <% onclick = "document.location='{url}'".format(url=url) %>
                    % else :
                        <% onclick = u"alert(\"Vous n'avez pas accès aux données de cet atelier\");" %>
                    % endif
                    <tr>
                        <td onclick="${onclick}" class="col_datetime">${api.format_date(workshop.datetime)}</td>
                        <td onclick="${onclick}" class="col_text">${workshop.name}</td>
                        <td onclick="${onclick}" class="col_text">
                            <ul>
                                % for trainer in workshop.trainers:
                                    <li>${trainer.label}</li>
                                % endfor
                            </ul>
                        </td>
                        <td onclick="${onclick}">${len(workshop.participants)}</td>
                        <td class="col_text">
                            % if request.has_permission('edit.workshop', workshop):
                                <ul>
                                    % for timeslot in workshop.timeslots:
                                        <li class="timeslot">
                                            <% pdf_url = request.route_path("timeslot.pdf", id=timeslot.id) %>
                                            <span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-pdf"></use></svg></span>
                                            <a href="${pdf_url}" title="Télécharger la feuille d’émargement au format PDF">
                                                % if workshop.relates_single_day():
                                                    ${api.format_datetime(timeslot.start_time, timeonly=True)} → \
                                                    ${api.format_datetime(timeslot.end_time, timeonly=True)} \
                                                    (${api.format_duration(timeslot.duration)})
                                                % else:
                                                    Du ${api.format_datetime(timeslot.start_time)} au \
                                                    ${api.format_datetime(timeslot.end_time)} \
                                                    (${api.format_duration(timeslot.duration)})
                                                % endif
                                            </a>
                                        </li>
                                    % endfor
                                </ul>
                            % else:
                                % for user in current_users:
                                    <% is_participant = workshop.is_participant(user.id) %>
                                    % if is_participant:
                                        ${api.format_account(user)} :
                                        % for timeslot in workshop.timeslots:
                                            <div>
                                                % if workshop.relates_single_day():
                                                    ${api.format_datetime(timeslot.start_time, timeonly=True)} → \
                                                    ${api.format_datetime(timeslot.end_time, timeonly=True)} : \
                                                % else:
                                                    Du ${api.format_datetime(timeslot.start_time)} \
                                                    au ${api.format_datetime(timeslot.end_time)} : \
                                                % endif
                                                ${timeslot.user_status(user.id)}
                                            </div>
                                        % endfor
                                    % endif
                                % endfor
                            % endif
                        </td>
                        <td 
                            % if request.has_permission('edit.workshop', workshop):
                        	class="col_actions width_two"
                        	% else: 
                        	class="col_actions width_one"
                        	% endif
                        	>
                            % if request.has_permission('edit.workshop', workshop):
                                <% edit_url = request.route_path('workshop', id=workshop.id, _query=dict(action="edit")) %>
                                ${table_btn(edit_url, u"Voir/éditer", u"Voir / Éditer l'atelier", icon='pen')}
                                <% del_url = request.route_path('workshop', id=workshop.id, _query=dict(action="delete")) %>
                                ${table_btn(del_url, u"Supprimer",  u"Supprimer cet atelier", icon='trash-alt', method='post', \
                                onclick=u"return confirm('Êtes vous sûr de vouloir supprimer cet atelier ?')", css_class="negative")}
                            % elif request.has_permission("view.workshop", workshop):
                                ${table_btn(url, u"Voir", u"Voir l'atelier", icon='arrow-right')}
                            % endif
                        </td>
                    </tr>
                % endfor
                % if len(records) == 0:
                    <td colspan="6" class="col_text"><em>Aucun atelier ne correspond à votre recherche</em></td>
                % endif
            </tbody>
        </table>
    </div>
    ${pager(records)}
</div>
</%block>
