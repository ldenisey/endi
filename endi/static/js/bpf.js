/** Show subcontract details only if there are subcontracts
 */
class SubContractFields {
    constructor(parentSelector, childSelectors) {
        this.parentSelector = parentSelector;
        this.childSelectors = childSelectors;
        this.update();
        this.install();
    }
    install() {
        let self = this;
        $(this.parentSelector).change(function() {
            self.update();
        });
    }
    update() {
        let parentVal = $(this.parentSelector).val();
        for (let i of this.childSelectors) {
            let formGroup = $(i).parent('.form-group');
            if (parentVal == 'no') {
                $(i).val(0);
                formGroup.hide();
            } else {
                formGroup.show();
            }
        }
    }
}

/** Show trainee & hours count only if several trainee types
 */
class TraineeTypeFields {
    constructor(parentSelector, childSelectors) {
        this.parentSelector = parentSelector;
        this.childSelectors = childSelectors;
        this.update();
        this.install();
    }
    install() {
        let self = this;
        let deformSeq = $(this.parentSelector).parentsUntil('.deform-seq');
        console.log(deformSeq);
        let seqAddButton = deformSeq.find('.deform-seq-add');
        seqAddButton.click(function() {self.update()});
    }
    update() {
        let parentOccurences = $(this.parentSelector+':visible').length;
        for (let i of this.childSelectors) {
            let formGroup = $(i).parentsUntil('.deform-seq-item','.form-group');
            if (parentOccurences == 1) {
                formGroup.hide();
                $(i).val(0); // backend will fill it appropriately
            } else {
                formGroup.show();
            }
        }
    }
}
