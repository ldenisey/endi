# -*- coding: utf-8 -*-
import logging
import datetime

from sqlalchemy import extract

from endi.scripts.utils import (
    command,
    get_value,
    has_value,
)
from endi.models.task import (
    Task,
)
from endi_base.models.base import DBSESSION as db
from endi.models.task.task import cache_amounts


def refresh_task_amount_command(arguments, env):
    """
    Refresh the task amount cache

    :param dict arguments: docopt parsed arguments
    :param dict env: The pyramid env
    """
    logger = logging.getLogger(__name__)
    if not arguments['refresh']:
        logger.exception(u"Unknown error")

    logger.debug(u"Refreshing cached Task amounts")
    session = db()
    index = 0
    types = get_value(arguments, '--type')
    if types is None:
        types = ['invoice', 'estimation', 'cancelinvoice']

    this_year = datetime.date.today().year

    for task in Task.query().filter(
        Task.type_.in_(types)
    ).filter(extract('year', Task.date) == this_year):
        try:
            cache_amounts(None, None, task)
            session.merge(task)
            index += 1
            if index % 200 == 0:
                logger.debug('flushing')
                session.flush()
        except:
            logger.exception(u"Error while caching total : {0}".format(task.id))


def refresh_task_pdf_command(arguments, env):
    """
    Refresh task's pdf cached files

    .. code-block: command

        $ endi-cache app.ini refresh_task_pdf --all=1
        $ endi-cache app.ini refresh_task_pdf --companies=1,2

    :param dict arguments: docopt parsed arguments
    :param dict env: The pyramid env
    """
    from endi.models.company import Company
    from endi.models.files import File
    logger = logging.getLogger(__name__)

    session = db()
    allcompanies = has_value(arguments, 'all')
    if allcompanies:
        companies = [c[0] for c in session.query(Company.id)]
    else:
        companies = get_value(arguments, 'companies', "").split(',')
        companies = [int(id_) for id_ in companies]

    if not companies:
        raise Exception(u"Missing mandatory --companies argument (or --all)")

    logger.debug("Cleaning cache for {}".format(companies))
    from endi.models.task import Task
    pdf_ids = []
    tasks = Task.query().filter(
        Task.status == 'valid'
    ).filter(
        Task.company_id.in_(companies)
    ).filter(Task.pdf_file_id != None).all()

    for task in tasks:
        pdf_ids.append(task.pdf_file_id)
        task.pdf_file = None
        session.merge(task)

    session.flush()

    for file_ in pdf_ids:
        session.delete(File.get(file_))


def cache_entry_point():
    """Test migration of costs

    Usage:
        endi-cache <config_uri> refresh_task_amount [--type=<type>]
        endi-cache <config_uri> refresh_task_pdf [--companies=<companies>] [--all=<allcompanies>]

    o refresh : Ask for a cache refresh

    Options:
        -h --help               Show this screen
        --type=<type>           Only refresh cache for a given type
        (estimation/invoice/cancelinvoice)
        --companies=<companies> Comma separated list of company ids
        --all=<allcompanies>    set value to 1 to treat all companies
    """
    def callback(arguments, env):
        if arguments['refresh_task_amount']:
            func = refresh_task_amount_command
        elif arguments['refresh_task_pdf']:
            func = refresh_task_pdf_command
        return func(arguments, env)
    try:
        return command(callback, cache_entry_point.__doc__)
    finally:
        pass
