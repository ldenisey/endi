import pytest

@pytest.fixture
def mk_task(
        fixture_factory,
        user, company,
        project,
        customer,
):
    from endi.models.task.task import Task
    return fixture_factory(
        Task,
        user=user,
        company=company,
        project=project,
        customer_id=customer.id,
        customer=customer,
    )


@pytest.fixture
def mk_estimation(
        fixture_factory,
        user, company,
        project,
        customer,
):
    from endi.models.task.estimation import Estimation
    return fixture_factory(
        Estimation,
        user=user,
        company=company,
        project=project,
        customer_id=customer.id,
        customer=customer,
    )


@pytest.fixture
def mk_invoice(
    fixture_factory,
    project,
    customer,
    company,
    user,
    phase,
):
    from endi.models.task.invoice import Invoice
    return fixture_factory(
        Invoice,
        company=company,
        project=project,
        customer=customer,
        phase=phase,
        user=user,
    )


@pytest.fixture
def mk_cancel_invoice(
    fixture_factory,
    project,
    customer,
    company,
    user,
    phase,
):
    from endi.models.task.invoice import CancelInvoice
    return fixture_factory(
        CancelInvoice,
        company=company,
        project=project,
        customer=customer,
        phase=phase,
        user=user,
    )


@pytest.fixture
def mk_payment_line(
    fixture_factory,
):
    from endi.models.task.estimation import PaymentLine
    return fixture_factory(
        PaymentLine,
        description=u"Payment Line",
    )


@pytest.fixture
def payment_line_1(mk_payment_line):
    return mk_payment_line(amount=4000000)


@pytest.fixture
def payment_line_2(mk_payment_line):
    return mk_payment_line(amount=6000000)


@pytest.fixture
def payment_line_3(mk_payment_line):
    return mk_payment_line(amount=50000)


@pytest.fixture
def empty_task(mk_task):
    return mk_task(mode='ttc')


@pytest.fixture
def empty_ht_estimation(mk_estimation):
    return mk_estimation(mode='ht')


@pytest.fixture
def cancelinvoice_1(
        mk_cancel_invoice,
        project,
        customer,
        company,
        user,
        phase,
):
    return mk_cancel_invoice(status='valid')


@pytest.fixture
def cancelinvoice_2(
        mk_cancel_invoice,
        project,
        customer,
        company,
        user,
        phase,
):
    return mk_cancel_invoice(status='valid')


@pytest.fixture
def invoice_ht_mode(mk_invoice):
    return mk_invoice(mode='ht')


@pytest.fixture
def payment_one(dbsession):
    from endi.models.task.invoice import Payment
    payment = Payment(
        amount=1500000,
    )
    return payment


@pytest.fixture
def payment_two(dbsession):
    from endi.models.task.invoice import Payment
    payment = Payment(
        amount=1000000,
    )
    return payment
