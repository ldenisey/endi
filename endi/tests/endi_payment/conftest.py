# -*- coding: utf-8 -*-
import datetime

from mock import MagicMock
import pytest


@pytest.fixture
def invoice_base_config(dbsession):
    from endi.models.config import Config
    Config.set('invoice_number_template', '{SEQYEAR}')


@pytest.fixture
def product_without_tva(dbsession):
    from endi.models.tva import Product
    product = Product(name='product', compte_cg='122')
    dbsession.add(product)
    dbsession.flush()
    return product


@pytest.fixture
def task_line_group(dbsession):
    from endi.models.task.task import TaskLineGroup
    group = TaskLineGroup(
        order=1,
        title=u"Group title",
        description=u"Group description",
    )
    dbsession.add(group)
    dbsession.flush()
    return group


@pytest.fixture
def task_line(dbsession, unity, tva, product, task_line_group):
    from endi.models.task.task import TaskLine
    # TTC = 120 €
    line = TaskLine(
        cost=10000000,
        quantity=1,
        unity=unity.label,
        tva=tva.value,
        product_id=product.id,
        group=task_line_group,
    )
    dbsession.add(line)
    dbsession.flush()
    return line


@pytest.fixture
def discount_line(dbsession, tva):
    from endi.models.task.task import DiscountLine
    discount = DiscountLine(
        description="Discount", amount=1000000, tva=tva.value
    )
    dbsession.add(discount)
    dbsession.flush()
    return discount


@pytest.fixture
def mk_invoice(
    dbsession,
    tva,
    unity,
    project,
    customer,
    company,
    user,
    phase,
):
    def _mk_invoice(date=None, company=company):
        from endi.models.task.invoice import Invoice
        invoice = Invoice(
            company=company,
            project=project,
            customer=customer,
            phase=phase,
            user=user,
            date=date,
        )
        dbsession.add(invoice)
        dbsession.flush()
        return invoice
    return _mk_invoice


@pytest.fixture
def invoice(mk_invoice):
    return mk_invoice()

@pytest.fixture
def full_invoice(
    dbsession, invoice, task_line_group, task_line, user, mention, discount_line
):
    # TTC  : 120 - 12  + 12 €
    task_line_group.lines = [task_line]
    invoice.line_groups = [task_line_group]
    invoice.discounts = [discount_line]
    invoice.workplace = u'workplace'
    invoice.mentions = [mention]
    invoice.expenses_ht = 1000000
    invoice = dbsession.merge(invoice)
    dbsession.flush()
    return invoice
