# -*- coding: utf-8 -*-
import pytest
import datetime


@pytest.fixture
def bank_remittance(dbsession, bank, user, mode):
    from endi.models.task.payment import BankRemittance
    remittance = BankRemittance(
        id="REM_ID",
        payment_mode=mode.label,
        bank_id=bank.id,
        remittance_date="2019-01-01",
        closed=1,
    )
    dbsession.add(remittance)
    dbsession.flush()
    return remittance


@pytest.fixture
def payment(
    dbsession, bank, full_invoice, user, mode, bank_remittance, customer_bank
):
    from endi.models.task.payment import Payment
    payment = Payment(
        amount=15*10**5,
        bank_id=bank.id,
        user_id=user.id,
        task_id=full_invoice.id,
        bank_remittance_id=bank_remittance.id,
        mode=mode.label,
        customer_bank_id=customer_bank.id,
        check_number="0123456789",
        issuer=full_invoice.customer.label,
    )
    dbsession.add(payment)
    dbsession.flush()
    return payment


def test_payment_add(full_invoice, get_csrf_request_with_db, user, bank):
    request = get_csrf_request_with_db(user=user)
    from endi_payment.public import PaymentService
    service = PaymentService(None, request)

    params = {'amount': 2000000, 'mode': 'cheque', "bank_id": bank.id}
    service.add(full_invoice, params)

    assert len(full_invoice.payments) == 1
    assert full_invoice.payments[0].amount == 2000000


def test_payment_update(payment, get_csrf_request_with_db, user):
    request = get_csrf_request_with_db(user=user)
    from endi_payment.public import PaymentService
    service = PaymentService(None, request)

    params = {'amount': 2000000, 'mode': 'cheque'}
    service.update(payment, params)

    assert payment.mode == "cheque"
    assert payment.amount == 2000000


def test_payment_delete(payment, full_invoice, get_csrf_request_with_db, user):
    from endi.models.task.invoice import Payment
    payment_id = payment.id
    request = get_csrf_request_with_db(user=user)
    from endi_payment.public import PaymentService
    service = PaymentService(None, request)
    service.delete(payment)

    assert len(full_invoice.payments) == 0
    assert Payment.get(payment_id) == None
