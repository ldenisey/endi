# -*- coding: utf-8 -*-
import unittest
import locale
from endi.utils import strings


class TestIt(unittest.TestCase):
    def test_format_amount(self):
        a = 1525
        b = 1525.3
        locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')
        self.assertEqual(strings.format_amount(a), u"15,25")
        self.assertEqual(strings.format_amount(a, trim=False), u"15,25")

        self.assertEqual(strings.format_amount(b), u"15,25")
        self.assertEqual(strings.format_amount(b, trim=False), u"15,25")

        c = 210000
        self.assertEqual(
            strings.format_amount(c, grouping=False),
            u"2100,00"
        )
        self.assertEqual(
            strings.format_amount(c, grouping=True),
            u"2&nbsp;100,00"
        )

        c = 21000000.0
        self.assertEqual(
            strings.format_amount(c, trim=False, precision=5),
            u"210,00"
        )
        c = 21000004.0
        self.assertEqual(
            strings.format_amount(c, trim=False,precision=5),
            u"210,00004"
        )
        c = 21000040.0
        self.assertEqual(
            strings.format_amount(c, trim=False,precision=5),
            u"210,0004"
        )

        self.assertEqual(
            strings.format_amount(c, trim=True, precision=5),
            u"210,00"
        )
        c = 21012000.0
        self.assertEqual(
            strings.format_amount(c, trim=False, precision=5),
            u"210,12"
        )

        # With None input
        self.assertEqual(
            strings.format_amount(None),
            u""
        )

    def test_format_name(self):
        self.assertEqual(strings.format_name(None, u"LastName"),
                                                         u"LASTNAME ")
        self.assertEqual(strings.format_name(u"Firstname", None),
                                                        u" Firstname")

    def test_remove_kms_training_zeros(self):
        a = '12000'
        b = '14000,00'
        c = '16000,60'
        self.assertEqual(strings.remove_kms_training_zeros(a), u"12000")
        self.assertEqual(strings.remove_kms_training_zeros(b), u"14000")
        self.assertEqual(strings.remove_kms_training_zeros(c), u"16000,60")


def test_format_float():
    locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')
    from endi.utils.strings import format_float

    assert format_float(1.256, precision=2) == u"1,26"
    res = format_float(1265.254, precision=2, html=False)
    assert res == u"1\u202f265,25" or res == u"1 265,25"
    assert format_float(1265.254, precision=2) == u"1&nbsp;265,25"
    assert format_float(1265.254, precision=2, grouping=False) == u"1265,25"
    assert format_float(1.256, precision=None) == u"1.256"
