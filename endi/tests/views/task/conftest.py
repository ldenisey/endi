# -*- coding: utf-8 -*-
import pytest


@pytest.fixture
def pdf_config(dbsession, config):
    from endi.models.config import Config
    Config.set('coop_cgv', u"Conditions générales de vente")
    Config.set("coop_pdffootertitle", u"Pied de page")
    Config.set("coop_pdffootertext", u"Contenu du pied de page")
    Config.set("coop_pdffootercourse", u"Formation professionnelle")
    dbsession.flush()

    config.include("endi.panels.task")


@pytest.fixture
def task_line(mk_task_line, tva, product):
    # TTC = 120 €
    return mk_task_line(cost=10000000, tva=tva.value, product=product)


@pytest.fixture
def full_estimation(
    dbsession, estimation, task_line_group, task_line, user, mention,
    discount_line, payment_line, payment_line2, mk_task_line, mk_tva,
    mk_product
):
    tva = mk_tva(name="7%", value=700, default=False)
    product = mk_product(name='product7', tva=tva)
    # TTC  : 100 + 20/100 * 100 + 100 + 7%*100 - 12  + 12 €
    # accompte : 10/100
    # payments : 1er paiement de 150 + solde
    task_line_group.lines = [
        task_line,
        mk_task_line(cost=10000000, tva=700, product=product)
    ]
    estimation.deposit = 10
    estimation.line_groups = [task_line_group]
    estimation.discounts = [discount_line]
    estimation.payment_lines = [payment_line, payment_line2]
    estimation.workplace = u'workplace'
    estimation.mentions = [mention]
    estimation.expenses_ht = 1000000
    estimation = dbsession.merge(estimation)
    estimation.manualDeliverables = 1
    dbsession.flush()
    return estimation


@pytest.fixture
def business(dbsession, full_estimation, default_business_type):
    from endi.models.project.business import Business

    business = Business(name="business", business_type=default_business_type)
    business.estimations = [full_estimation]
    dbsession.add(business)
    dbsession.flush()
    full_estimation.business_type_id = default_business_type.id
    full_estimation.businesses = [business]
    dbsession.merge(full_estimation)
    return business
