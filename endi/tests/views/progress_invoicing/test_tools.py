# -*- coding: utf-8 -*-

from endi.views.progress_invoicing.tools import (
    AbstractProgressTaskLine,
    AbstractProgressTaskLineGroup,
)


class TestAbstractProgressTaskLine(object):
    def test__get_current_value(
        self,
        business,
        invoice,
        user,
    ):
        new_invoice = business.add_invoice(user)
        group_status = business.progress_invoicing_group_statuses[0]
        status = group_status.line_statuses[0]

        business.populate_progress_invoicing_lines(
            new_invoice,
            {group_status.id: {status.id: 15}}
        )
        item = AbstractProgressTaskLine(status, new_invoice.id)
        assert item._get_current_value() == 15.0

    def test__get_percent_left(self, business, invoice, user):
        new_invoice = business.add_invoice(user)
        group_status = business.progress_invoicing_group_statuses[0]
        status = group_status.line_statuses[0]

        business.populate_progress_invoicing_lines(
            new_invoice,
            {group_status.id: {status.id: 15}}
        )
        item = AbstractProgressTaskLine(status, new_invoice.id)
        assert item._get_percent_left() == 90

