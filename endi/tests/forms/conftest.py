# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import pytest


@pytest.fixture
def schema_node():
    import colander
    return colander.SchemaNode(colander.String())
