# -*- coding: utf-8 -*-
import colander
import pytest
import datetime
from endi.forms.tasks.invoice import (
    get_add_edit_invoice_schema,
    get_add_edit_cancelinvoice_schema,
)


def test_cancelinvoice_invoice_id():
    schema = get_add_edit_cancelinvoice_schema(includes=('invoice_id',))
    schema = schema.bind()

    value = {'invoice_id': 5}
    assert schema.deserialize(value) == value

    value = {}
    with pytest.raises(colander.Invalid):
        schema.deserialize(value)


def test_cancelinvoice(request_with_config, config, cancelinvoice, tva, unity):
    schema = get_add_edit_cancelinvoice_schema()
    request_with_config.context = cancelinvoice
    schema = schema.bind(request=request_with_config)

    value = {
        "name": u"Avoir 1",
        'date': datetime.date.today().isoformat(),
        'address': u"adress",
        "description": u"description",
        "payment_conditions": u"Test",
        'invoice_id': 5,
        'financial_year': 2017,
        'line_groups': [
            {
                'task_id': 5,
                'title': u"title",
                'description': u"description",
                "order": 5,
                'lines': [
                    {
                        'cost': 15,
                        'tva': 20,
                        'description': u'description',
                        'unity': u"Mètre",
                        "quantity": 5,
                        "order": 2,
                    }
                ]
            }
        ],
    }
    expected_value = {
        "name": u"Avoir 1",
        'date': datetime.date.today(),
        'address': u"adress",
        "description": u"description",
        "payment_conditions": u"Test",
        'invoice_id': 5,
        'financial_year': 2017,
        'line_groups': [
            {
                'task_id': 5,
                'title': u"title",
                'description': u"description",
                "order": 5,
                'lines': [
                    {
                        'cost': 1500000,
                        'tva': 2000,
                        'description': u'description',
                        'mode': 'ht',
                        'unity': u"Mètre",
                        "quantity": 5.0,
                        "order": 2,
                    }
                ]
            }
        ],
    }
    # Check those values are valid
    result = schema.deserialize(value)
    for key, value in expected_value.items():
        assert result[key] == value


def test_invoice(config, invoice, request_with_config, tva, unity):
    schema = get_add_edit_invoice_schema()
    request_with_config.context = invoice
    config.testing_securitypolicy(
        userid="test",
        groupids=('admin',),
        permissive=True
    )
    schema = schema.bind(request=request_with_config)

    value = {
        "name": u"Facture 1",
        'date': datetime.date.today().isoformat(),
        'address': u"adress",
        "description": u"description",
        "payment_conditions": u"Test",
        'estimation_id': 5,
        'financial_year': 2017,
        'line_groups': [
            {
                'task_id': 5,
                'title': u"title",
                'description': u"description",
                "order": 5,
                'lines': [
                    {
                        'cost': 15,
                        'tva': 20,
                        'description': u'description',
                        'unity': u"Mètre",
                        "quantity": 5,
                        "order": 2,
                    }
                ]
            }
        ],
    }
    expected_value = {
        "name": u"Facture 1",
        'date': datetime.date.today(),
        'address': u"adress",
        "description": u"description",
        "payment_conditions": u"Test",
        'estimation_id': 5,
        'financial_year': 2017,
        'line_groups': [
            {
                'task_id': 5,
                'title': u"title",
                'description': u"description",
                "order": 5,
                'lines': [
                    {
                        'cost': 1500000,
                        'tva': 2000,
                        'description': u'description',
                        'mode': 'ht',
                        'unity': u"Mètre",
                        "quantity": 5.0,
                        "order": 2,
                    }
                ]
            }
        ],
    }
    # Check those values are valid
    result = schema.deserialize(value)
    for key, value in expected_value.items():
        assert result[key] == value
