from endi.events.business import update_business_bpf_indicator


def test_update_bpf_status_indicator_issue_1743(
        business,  # not bpf related
):
    from endi.models.indicators import CustomBusinessIndicator

    update_business_bpf_indicator(None, None, business)

    bpf_indicator = CustomBusinessIndicator.query().filter_by(
        name="bpf_filled",
        business=business,
    ).first()

    assert bpf_indicator is None


def test_update_bpf_status_indicator(
        training_business,  # not bpf related
):
    from endi.models.indicators import CustomBusinessIndicator

    update_business_bpf_indicator(None, None, training_business)

    bpf_indicator = CustomBusinessIndicator.query().filter_by(
        name="bpf_filled",
        business=training_business,
    ).first()

    assert bpf_indicator is not None
