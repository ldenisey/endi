# -*- coding: utf-8 -*-
import pytest
import datetime

@pytest.fixture
def payment_line(mk_payment_line):
    return mk_payment_line(amount=15000000)


@pytest.fixture
def full_estimation(
    dbsession, estimation, task_line_group, task_line, user, mention,
    discount_line, payment_line, payment_line2, mk_task_line,
    mk_tva, mk_product
):
    tva = mk_tva(name="7%", value=700, default=False)
    product = mk_product(name='product7', tva=tva)
    # TTC  : 100 + 20/100 * 100 + 100 + 7%*100 - 12  + 12 €
    # accompte : 10/100
    # payments : 1er paiement de 150 + solde
    task_line_group.lines = [
        task_line,
        mk_task_line(cost=10000000, tva=700, product=product)
    ]
    estimation.deposit = 10
    estimation.display_units = 1
    estimation.line_groups = [task_line_group]
    estimation.discounts = [discount_line]
    estimation.payment_lines = [payment_line, payment_line2]
    estimation.workplace = u'workplace'
    estimation.mentions = [mention]
    estimation.expenses_ht = 1000000
    estimation = dbsession.merge(estimation)
    estimation.manualDeliverables = 1
    dbsession.flush()
    return estimation


@pytest.fixture
def full_invoice(
    dbsession, invoice, task_line_group, task_line, user, mention,
    discount_line
):
    # TTC  : 120 - 12  + 12 €
    task_line_group.lines = [task_line]
    invoice.line_groups = [task_line_group]
    invoice.discounts = [discount_line]
    invoice.workplace = u'workplace'
    invoice.mentions = [mention]
    invoice.expenses_ht = 1000000
    invoice = dbsession.merge(invoice)
    dbsession.flush()
    return invoice


@pytest.fixture
def business(dbsession, mk_business, full_estimation, default_business_type):
    business = mk_business()
    business.estimations = [full_estimation]
    dbsession.merge(business)
    full_estimation.business_type_id = default_business_type.id
    full_estimation.businesses = [business]
    dbsession.merge(full_estimation)
    dbsession.flush()
    return business


@pytest.fixture
def training_speciality(dbsession):
    from endi.models.training.bpf import NSFTrainingSpecialityOption
    ts = NSFTrainingSpecialityOption(label='maths')
    dbsession.add(ts)
    dbsession.flush()
    return ts


@pytest.fixture
def mk_business_bpf_data(dbsession, training_speciality):
    def _mk_business_bpf_data(business, financial_year):
        from endi.models.training.bpf import BusinessBPFData
        bpf_data = BusinessBPFData(
            business_id=business.id,
            financial_year=financial_year,
            cerfa_version='10443*15',
            total_hours=100,
            headcount=10,
            has_subcontract='no',
            has_subcontract_hours=0,
            has_subcontract_headcount=0,
            is_subcontract=False,
            training_speciality_id=training_speciality.id,
            training_goal_id=0,
        )
        dbsession.add(bpf_data)
        dbsession.flush()
        dbsession.refresh(business)
        return bpf_data

    return _mk_business_bpf_data


@pytest.fixture
def business_with_progress_invoicing(dbsession, business, full_estimation):
    from endi.models.services.business import BusinessService
    business.estimations[0].status = 'valid'
    business.invoicing_mode = business.PROGRESS_MODE
    # On crée les statut de facturation à l'avancement
    BusinessService.populate_progress_invoicing_status(business)
    dbsession.merge(business.estimations[0])
    dbsession.merge(business)
    dbsession.flush()
    return business


@pytest.fixture
def progress_invoicing_invoice(
    dbsession, business_with_progress_invoicing, user
):
    from endi.models.services.business import BusinessService
    invoice = business_with_progress_invoicing.add_invoice(user)

    # On construit la structure de données attendues pour la génération des
    # lignes de prestation
    appstruct = {}
    for status in business_with_progress_invoicing.\
            progress_invoicing_group_statuses:
        appstruct[status.id] = {}
        for line_status in status.line_statuses:
            appstruct[status.id][line_status.id] = 10
    # On populate notre facture
    BusinessService.populate_progress_invoicing_lines(
        business_with_progress_invoicing,
        invoice,
        appstruct,
    )
    dbsession.merge(invoice)
    dbsession.flush()
    return invoice
