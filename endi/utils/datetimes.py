# -*- coding:utf-8 -*-
import logging
import datetime

logger = logging.getLogger(__name__)


class UnicodeDate(datetime.date):
    """Tweaked class formatting as unicode rather than str

    Because original class behaviour (outputing bytes) causes that kind of call
    to fail if non-ascii char is present in month name ::

        u'{:%B}'.format(date(2018, 12, 12))

    NB: Using Python3 will make this un-necessary (date.__format__ natively
    outputs unicode).

    """
    def __format__(self, *args, **kwargs):
        ret = super(UnicodeDate, self).__format__(*args, **kwargs)
        return ret.decode('utf-8')


def parse_date(value, default=None, format_="%Y-%m-%d"):
    """
    Get a date object from a string

    :param str value: The string to parse
    :param str default: The default value to return
    :param str format_: The format of the str date
    :returns: a datetime.date object
    """
    try:
        result = datetime.datetime.strptime(value, format_).date()
    except ValueError as err:
        logger.debug(u"{} is not a date".format(value))
        if default is not None:
            result = default
        else:
            raise err
    return result


def get_current_year():
    return datetime.date.today().year
