# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from sqlalchemy import (
    Column,
    Integer,
    String,
)
from sqlalchemy.ext.declarative import declared_attr

from endi_base.models.mixins import DuplicableMixin
from endi.compute.math_utils import (
    integer_to_amount,
)


class LineModelMixin(DuplicableMixin, object):
    """
    Common fields between SupplierOrderLine and SupplierInvoiceLine
    """
    __duplicable_fields__ = [
        'ht',
        'tva',
        'description',
        'type_id',
    ]

    @declared_attr
    def description(cls):
        return Column(
            String(255),
            info={
                'colanderalchemy': {'title': "Description"}
            },
            default="",
        )

    @declared_attr
    def ht(cls):
        return Column(
            Integer,
            info={
                'colanderalchemy': {'title': 'Montant HT'},
            },
        )

    @declared_attr
    def tva(cls):
        return Column(
            Integer,
            info={
                'colanderalchemy': {'title': 'Montant de la TVA'}
            },
        )

    @declared_attr
    def type_id(cls):
        return Column(
            Integer,
            info={
                'colanderalchemy': {"title": "Type de dépense"}
            }
        )

    def __json__(self, request):
        return dict(
            id=self.id,
            type_id=self.type_id,
            ht=integer_to_amount(self.ht, 2),
            tva=integer_to_amount(self.tva, 2),
            description=self.description,
        )
