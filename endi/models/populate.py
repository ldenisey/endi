# -*- coding: utf-8 -*-
import logging
from transaction import commit
import sqlalchemy

from endi_base.models.base import DBSESSION


logger = logging.getLogger(__name__)

GROUPS = (
    {
        'name': 'manager',
        'label': u"Est membre de l'équipe d'appui",
        'primary': True,
    },
    {
        'name': 'admin',
        'label': u"Administre l'application",
        "primary": True,
    },
    {
        'name': 'contractor',
        'label': u'Entrepreneur de la coopérative',
        'primary': True
    },
    {
        'name': 'estimation_validation',
        'label': u"Peut valider ses propres devis",
    },
    {
        'name': 'invoice_validation',
        'label': u"Peut valider ses propres factures",
    },
    {
        'name': 'estimation_only',
        'label': u"Ne peut pas créer de factures sans devis",
    },
    {
        'name': "payment_admin",
        'label': u"Peut saisir/modifier/supprimer les paiements \
de ses factures",
    },
    {
        'name': 'trainer',
        'label': u"Formateur",
    },
    {
        'name': 'constructor',
        'label': u"Peut initier des chantiers",
    },
)


def populate_cae_situations_and_career_stages(session):
    """
    Populate the database with default CAE situation options and career stages
    """
    # Populate CAE situations
    from endi.models.user.userdatas import CaeSituationOption
    query = session.query(CaeSituationOption)
    if query.count() == 0:
        situation_cand = CaeSituationOption(label=u"Candidat", order=0)
        situation_conv = CaeSituationOption(
            label=u"En convention",
            is_integration=True,
            order=1
        )
        situation_es = CaeSituationOption(
            label=u"Entrepreneur salarié",
            is_integration=True,
            order=2
        )
        situation_out = CaeSituationOption(label=u"Sortie", order=3)
        session.add(situation_cand)
        session.add(situation_conv)
        session.add(situation_es)
        session.add(situation_out)
        session.flush()
    # Populate Career Stages
    from endi.models.career_stage import CareerStage
    if CareerStage.query().count() == 0:
        for active, name, cae_situation_id, stage_type in (
            (True, "Diagnostic", None, None),
            (True, "Contrat CAPE", situation_conv.id, "entry"),
            (True, "Contrat CESA", situation_es.id, "contract"),
            (True, "Avenant contrat", None, "amendment"),
            (True, "Sortie", situation_out.id, "exit"),
        ):
            session.add(
                CareerStage(
                    active=active,
                    name=name,
                    cae_situation_id=cae_situation_id,
                    stage_type=stage_type,
                )
            )
        session.flush()


def populate_training_types(session):
    """
    Populate the database with default training type options
    """
    from endi.models.sale_product.training import TrainingTypeOptions
    query = session.query(TrainingTypeOptions)
    if query.filter(TrainingTypeOptions.label == "Individuelle").count() == 0:
        session.add(TrainingTypeOptions(label=u"Individuelle", order=0))
    if query.filter(TrainingTypeOptions.label == "Groupe").count() == 0:
        session.add(TrainingTypeOptions(label=u"Groupe", order=1))
    if query.filter(
        TrainingTypeOptions.label == "Petit groupe (moins de 5 personnes)"
    ).count() == 0:
        session.add(
            TrainingTypeOptions(
                label=u"Petit groupe (moins de 5 personnes)",
                order=2
            )
        )
    if query.filter(TrainingTypeOptions.label == "Sur mesure").count() == 0:
        session.add(TrainingTypeOptions(label=u"Sur mesure", order=3))
    if query.filter(TrainingTypeOptions.label == "À distance").count() == 0:
        session.add(TrainingTypeOptions(label=u"À distance", order=4))
    session.flush()


def populate_groups(session):
    """
    Populate the groups in the database
    """
    from endi.models.user.group import Group
    for group in GROUPS:
        if session.query(Group.id).filter(
            Group.name == group['name']
        ).count() == 0:
            session.add(Group(**group))
    session.flush()


def populate_accounting_treasury_measure_types(session):
    """
    Populate the database with treasury measure types
    """
    from endi.models.config import Config
    from endi.models.accounting.treasury_measures import (
        TreasuryMeasureTypeCategory,
        TreasuryMeasureType,
    )
    if session.query(TreasuryMeasureTypeCategory.id).count() == 0:
        categories = []
        for order, name in enumerate(
            [u"Référence", u"Future", u"Autres"]
        ):
            category = TreasuryMeasureTypeCategory(label=name, order=order)
            session.add(category)
            session.flush()
            categories.append(category.id)

        types = [
            (0, '5', u"Trésorerie du jour", True, "account_prefix"),
            (0, "42,-421,-425,43,44", u"Impôts, taxes et cotisations dues",
             False, None),
            (0, "40", u"Fournisseurs à payer", False, None),
            (0, u"Référence", u"Trésorerie de référence", True, "categories"),
            (1, "421", u"Salaires à payer", False, None),
            (1, "41", u"Clients à encaisser", False, None),
            (1, '425', u"Notes de dépenses à payer", False, None),
            (1, "{Référence}+{Future}", u"Trésorerie future", True,
             "complex_total"),
            (2, "1,2,3", u"Comptes bilan non pris en compte", False, None),
            (2, "{Référence}+{Future}+{Autres}", u"Résultat de l'enseigne",
             True, "complex_total"),
        ]
        for order, data in enumerate(types):
            (
                category_index, account_prefix, label, is_total, total_type
            ) = data
            category_id = categories[category_index]
            session.add(
                TreasuryMeasureType(
                    category_id=category_id,
                    account_prefix=account_prefix,
                    label=label,
                    is_total=is_total,
                    order=order,
                    total_type=total_type
                )
            )
        if not Config.get_value("treasury_measure_ui"):
            Config.set("treasury_measure_ui", u"Trésorerie du jour")
        session.flush()


def populate_accounting_income_statement_measure_types(session):
    """
    Populate the database with treasury measure types
    """
    from endi.models.accounting.income_statement_measures import (
        IncomeStatementMeasureTypeCategory,
    )

    if session.query(IncomeStatementMeasureTypeCategory.id).count() == 0:
        for order, category in enumerate((
            u"Produits",
            u"Achats",
            u"Charges",
            u"Salaires et Cotisations"
        )):
            session.add(
                IncomeStatementMeasureTypeCategory(label=category, order=order)
            )
        session.flush()


def populate_bookentry_config(session):
    from endi.models.config import Config
    initial_values = [
        (
            'bookentry_facturation_label_template',
            '{invoice.customer.label} {company.name}'
        ),
        (
            'bookentry_contribution_label_template',
            "{invoice.customer.label} {company.name}"
        ),
        (
            'bookentry_rg_interne_label_template',
            "RG COOP {invoice.customer.label} {company.name}"
        ),
        (
            'bookentry_rg_client_label_template',
            "RG {invoice.customer.label} {company.name}"
        ),
        (
            'bookentry_expense_label_template',
            "{beneficiaire}/frais {expense_date:%-m %Y}"
        ),
        (
            'bookentry_payment_label_template',
            "{company.name} / Rgt {invoice.customer.label}"
        ),
        (
            'bookentry_expense_payment_main_label_template',
            "{beneficiaire_LASTNAME} / REMB FRAIS {expense_date:%B/%Y}"
        ),
        (
            'bookentry_expense_payment_waiver_label_template',
            "Abandon de créance {beneficiaire_LASTNAME} {expense_date:%B/%Y}"
        ),
        (
            'bookentry_supplier_invoice_label_template',
            "{company.name} / Fact. {supplier.label}"
        ),
        (
            'bookentry_supplier_payment_label_template',
            "{company.name} / Rgt {supplier.label}"
        ),
    ]
    for key, val in initial_values:
        if not Config.get_value(key):
            Config.set(key, val)


def populate_project_types(session):
    from endi.models.project.types import (
        ProjectType,
        BusinessType,
    )
    for name, label, subtype_label, private, default, include_price_study in (
        ("default", u"Dossier classique", "Affaire simple", False, True, False),
        (
            "training", u"Convention de formation", "Formation", True,
            False, False
        ),
        ("construction", u"Chantier", u"Chantier", True, False, True),
    ):
        ptype = ProjectType.query().filter_by(name=name).first()
        if ptype is None:
            ptype = ProjectType(
                name=name,
                label=label,
                editable=False,
                private=private,
                default=default,
                include_price_study=include_price_study,
            )
            session.add(ptype)
            session.flush()
            if name is not 'default':
                default_btype = BusinessType.query().filter_by(
                    name='default').first()
                default_btype.other_project_types.append(ptype)
                session.merge(default_btype)
                session.flush()

        if session.query(BusinessType.id).filter_by(name=name).count() == 0:
            session.add(
                BusinessType(
                    name=name,
                    label=subtype_label,
                    editable=False,
                    private=private,
                    project_type_id=ptype.id,
                )
            )
    session.flush()


def populate_contract_types(session):
    """
    Populate the database with default contract types
    """
    from endi.models.career_path import TypeContratOption
    query = session.query(TypeContratOption)
    if query.filter(TypeContratOption.label == "CDD").count() == 0:
        session.add(TypeContratOption(label=u"CDD", order=0))
    if query.filter(TypeContratOption.label == "CDI").count() == 0:
        session.add(TypeContratOption(label=u"CDI", order=0))
    if query.filter(TypeContratOption.label == "CESA").count() == 0:
        session.add(TypeContratOption(label=u"CESA", order=0))
    session.flush()


def _add_filetype_and_reqs(
    session, business_type_label, filetype, requirements
):
    """
    """
    from endi.models.files import FileType
    from endi.models.project.types import BusinessType
    from endi.models.project.file_types import BusinessTypeFileType
    if session.query(FileType.id).filter_by(label=filetype).count() == 0:
        f = FileType(label=filetype)
        session.add(f)
        session.flush()
        btype_id = session.query(BusinessType.id).filter_by(
            name=business_type_label
        ).scalar()

        for req_dict in requirements:
            req = BusinessTypeFileType(
                file_type_id=f.id,
                business_type_id=btype_id,
                doctype=req_dict['doctype'],
                requirement_type=req_dict['req_type'],
                validation=req_dict.get('validation', False)
            )
            session.add(req)
        session.flush()


def populate_file_types_and_requirements(session):
    """
    Add default file types to the database
    """
    filetype = "Formation : Convention"
    requirements = [
        {
            'doctype': 'business',
            'req_type': 'project_mandatory',
            'validation': True
        },
        {
            'doctype': 'invoice',
            'req_type': 'project_mandatory',
        },
    ]
    _add_filetype_and_reqs(session, "training", filetype, requirements)
    filetype = "Formation : Émargement"
    requirements = [
        {
            'doctype': 'business',
            'req_type': 'business_mandatory',
            'validation': True
        },
        {
            'doctype': 'invoice',
            'req_type': 'business_mandatory',
        },
    ]
    _add_filetype_and_reqs(session, "training", filetype, requirements)
    filetype = "Document fournisseur : Facture"
    requirements = [
    ]
    _add_filetype_and_reqs(session, "supplier_order", filetype, requirements)
    filetype = "Document fournisseur : Devis"
    requirements = [
    ]
    _add_filetype_and_reqs(session, "supplier_order", filetype, requirements)


def populate_invoice_number_template(session):
    from endi.models.config import Config
    if not Config.get_value("invoice_number_template"):
        Config.set("invoice_number_template", "{SEQGLOBAL}")
    session.flush()


def populate_banks(session):
    """
    Populate the banks in the database
    """
    from endi.models.payments import Bank
    if session.query(Bank.id).count() == 0:
        for order, bank_name in enumerate((
            u"Allianz Banque",
            u"Axa Banque",
            u"Banque Courtois",
            u"Banque de France",
            u"Banque Delubac",
            u"Banque Populaire",
            u"BARCLAYS",
            u"BFM",
            u"BNP",
            u"Boursorama Banque",
            u"BPCE",
            u"BTP Banque",
            u"Caisse d'Epargne",
            u"CIC",
            u"Crédit Agricole",
            u"Crédit Coopératif",
            u"Crédit du Nord",
            u"Crédit Mutuel",
            u"HSBC",
            u"ING Direct",
            u"La Banque Postale",
            u"La NEF",
            u"LCL",
            u"Société Générale",
            u"Société Marseillaise de Crédit",
        )):
            session.add(Bank(label=bank_name, order=order))
        session.flush()


def populate_database():
    """
    Populate the database with default values
    """
    logger.debug("Populating the database")
    session = DBSESSION()
    for func in (
        populate_cae_situations_and_career_stages,
        populate_groups,
        populate_accounting_treasury_measure_types,
        populate_accounting_income_statement_measure_types,
        populate_bookentry_config,
        populate_project_types,
        populate_contract_types,
        populate_file_types_and_requirements,
        populate_invoice_number_template,
        populate_training_types,
        populate_banks,
    ):
        try:
            func(session)
        except sqlalchemy.exc.OperationalError as e:
            print("There is an error in the population process :")
            print(e)
    commit()
