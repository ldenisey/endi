# -*- coding: utf-8 -*-
import datetime

from sqlalchemy import (
    Column,
    DateTime,
    Integer,
    Boolean,
    String,
    ForeignKey,
)
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import (
    relationship,
)
from endi_base.models.base import (
    DBBASE,
    default_table_args,
)
from endi.models.options import (
    ConfigurableOption,
    get_id_foreignkey_col,
)
from endi import forms


class PaymentModelMixin(object):
    """
    Common fields for payment various payment models
    """
    @declared_attr
    def mode(cls):
        return Column(String(50))

    @declared_attr
    def amount(cls):
        return Column(
            Integer,
            info={'colanderalchemy': {'title': u"Montant"}},
        )
    precision = 2

    @declared_attr
    def date(cls):
        return Column(
            DateTime(),
            default=datetime.datetime.now,
            info={'colanderalchemy': {'title': u"Date de remise"}},
        )

    @declared_attr
    def bank_remittance_id(cls):
        return Column(
            String(255),
            info={
                'colanderalchemy': {
                    'title': u"Identifiant de remise en banque"
                }
            },
            nullable=True
        )

    @declared_attr
    def exported(cls):
        return Column(Boolean(), default=False)

    # Non-database fields

    @property
    def parent(self):
        raise NotImplementedError

    def get_amount(self):
        return self.amount


class PaymentMode(DBBASE):
    """
        Payment mode entry
    """
    __colanderalchemy_config__ = {
        "title": u"Modes de paiement",
        "description": u"Configurer les modes de paiement proposés dans les \
formulaires d'enregistrement des encaissements",
        "seq_widget_options": {
            "add_subitem_text_template": u"Ajouter un mode de paiement",
        },
        "help_msg": u"Configurer les modes de paiement pour la saisie des \
encaissements des factures",
        "validation_msg": u"Les modes de paiement ont bien été configurés"
    }
    __tablename__ = "paymentmode"
    __table_args__ = default_table_args
    id = Column(
        Integer,
        primary_key=True,
        info={'colanderalchemy': forms.get_hidden_field_conf()},
    )
    label = Column(
        String(120),
        info={'colanderalchemy': {'title': u"Libellé"}}
    )


class BankAccount(ConfigurableOption):
    """
    Bank accounts used for payment registry
    """
    __colanderalchemy_config__ = {
        "title": u"Comptes bancaires",
        "seq_widget_options": {
            "add_subitem_text_template": u"Ajouter un compte bancaire",
        },
        'validation_msg': u"Les comptes bancaires ont bien été configurés",
    }
    id = get_id_foreignkey_col('configurable_option.id')
    code_journal = Column(
        String(120),
        info={
            "colanderalchemy": {
                'title': u"Code journal Banque",
                'description': u"""Code journal utilisé pour les exports
                des encaissements et des règlements des notes de dépense""",
            }
        },
        nullable=False,
    )
    compte_cg = Column(
        String(120),
        info={"colanderalchemy": {'title': u"Compte général Banque"}},
        nullable=False,
    )
    iban = Column(
        String(35),
        info={"colanderalchemy": {'title': u"IBAN"}},
        nullable=True,
    )
    bic = Column(
        String(15),
        info={"colanderalchemy": {'title': u"BIC"}},
        nullable=True,
    )
    default = Column(
        Boolean(),
        default=False,
        info={"colanderalchemy": {'title': u"Utiliser ce compte par défaut"}}
    )
    payments = relationship(
        'Payment',
        order_by="Payment.date",
        info={'colanderalchemy': {'exclude': True}},
    )

    @property
    def rib_bank_code(self):
        # code établissement du RIB
        if self.iban:
            return self.iban[4:9]
        else:
            return ""

    @property
    def rib_bank_office(self):
        # code guichet du RIB
        if self.iban:
            return self.iban[9:14]
        else:
            return ""

    @property
    def rib_account_number(self):
        # numéro de compte du RIB
        if self.iban:
            return self.iban[14:-2]
        else:
            return ""

    @property
    def rib_account_key(self):
        # clé du RIB
        if self.iban:
            return self.iban[-2:]
        else:
            return ""


class Bank(ConfigurableOption):
    """
    Third parties bank list
    """
    __colanderalchemy_config__ = {
        "title": u"Banques clients",
        "seq_widget_options": {
            "add_subitem_text_template": u"Ajouter une banque",
        },
        'validation_msg': u"Les banques clients ont bien été configurés",
    }
    id = get_id_foreignkey_col('configurable_option.id')
