# -*- coding: utf-8 -*-
"""
    The third_party package entry
"""
from .third_party import ThirdParty
from .customer import Customer
from .supplier import Supplier
