# -*- coding: utf-8 -*-
"""
    Models for work unit (days, ...)
"""
from sqlalchemy import Column
from sqlalchemy import String
from sqlalchemy import Integer

from endi_base.models.base import default_table_args
from endi_base.models.base import DBBASE
from endi.forms import (
    get_hidden_field_conf,
)


class WorkUnit(DBBASE):
    """
        Work unit, used to build the price list
    """
    __colanderalchemy_config__ = {
        "title": u"Unités de prestation",
        "description": u"Configurer les unités de prestation proposées dans les \
formulaires d'édition des devis/factures",
        "help_msg": u"Configurer les unités de prestation proposées dans les \
formulaires d'édition des devis/factures",
        "validation_msg": u"Les unités de prestation ont bien été configurées",
        "seq_widget_options": {
            "add_subitem_text_template": u"Ajouter une unité de prestation"
        },
    }
    __tablename__ = "workunity"
    __table_args__ = default_table_args
    id = Column(
        Integer,
        primary_key=True,
        info={'colanderalchemy': get_hidden_field_conf()},
    )
    label = Column(
        String(100),
        info={'colanderalchemy': {'title': u"Intitulé"}}
    )

    def __json__(self, request):
        return dict(
            id=self.id,
            label=self.label,
            value=self.label
        )
