# -*- coding: utf-8 -*-
"""
    Regouping all models imports is necessary
    to allow the metadata.create_all function to work well
"""
from endi_base.models.base import DBBASE
from endi_base.models.base import DBSESSION

import activity
import career_stage
import career_path
import commercial
import company
import competence
import config
import files
import holiday
import indicators
import node
import options
import payments
import statistics
import supply
import third_party
import treasury
import tva
import task
import workshop
from .accounting import operations
from .accounting import treasury_measures
from .accounting import income_statement_measures
from .expense import sheet
from .expense import types
from .expense import payment
from .project import business
from .project import file_types
from .project import mentions
from .project import phase
# Évite les conflits de chemin lors d'import depuis pshell
from .project import project as p
from .project import types
# Évite les conflits de chemin lors d'import depuis pshell
from .progress_invoicing import progress_invoicing as prinv
from .sale_product import category
from .sale_product import base
# Évite les conflits de chemin lors d'import depuis pshell
from .sale_product import sale_product as s
from .sale_product import work
from .sale_product import work_item
from .sale_product import training
from .price_study import work
from .price_study import work_item
from .price_study import product
from .price_study import discount
# Évite les conflits de chemin lors d'import depuis pshell
from .price_study import price_study as p
# from .sale_product import price_study_work
from .training import trainer
from .training import bpf
# Évite les conflits de chemin lors d'import depuis pshell
from .user import user as u
from .user import login
from .user import group
from .user import userdatas
from endi_celery import models


def adjust_for_engine(engine):
    """
    Ajust the models definitions to fit the current database engine
    :param obj engine: The current engine to be used
    """
    if engine.dialect.name == 'mysql':
        # Mysql does case unsensitive comparison by default
        login.Login.__table__.c.login.type.collation = 'utf8_bin'
