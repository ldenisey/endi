# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
import logging

from pyramid.httpexceptions import HTTPFound
import deform_extensions

from endi import forms
from endi.forms.supply.supplier_invoice import (
    SupplierInvoiceAddSchema,
    SupplierPaymentSchema,
    SupplierInvoiceDispatchSchema,
)
from endi.forms.supply.supplier_invoice import get_supplier_invoice_list_schema
from endi.events.files import (
    FileAdded,
)
from endi.models.company import Company
from endi.models.files import File
from endi.models.supply import (
    SupplierInvoice,
    SupplierInvoiceLine,
    SupplierOrder,
    SupplierPayment,
)
from endi.models.third_party.supplier import Supplier
from endi.resources import (
    dispatch_supplier_invoice_js,
    supplier_invoice_resources,
)
from endi.utils.widgets import (
    Link,
    POSTButton,
    ViewLink,
)
from endi.views import (
    BaseListView,
    BaseFormView,
    BaseView,
    DeleteView,
    submit_btn,
)
from endi.views.files import FileUploadView
from endi.views.supply import SupplierDocListTools


logger = logging.getLogger(__name__)


def populate_actionmenu(request):
    return request.actionmenu.add(
        ViewLink(
            "Revenir à la liste des factures fournisseur",
            path="/company/{id}/suppliers_invoices",
            id=request.context.get_company_id(),
        )
    )


class SupplierInvoiceAddView(BaseFormView):
    add_template_vars = ('title',)
    title = "Saisir une facture fournisseur"

    schema = SupplierInvoiceAddSchema()
    buttons = (submit_btn,)

    def before(self, form):
        assert self.context.__name__ == 'company'

    def submit_success(self, appstruct):
        assert self.context.__name__ == 'company'
        appstruct['company_id'] = self.context.id

        suppliers_orders_ids = list(
            appstruct.pop('suppliers_orders_ids', set())
        )
        if len(suppliers_orders_ids) > 0:
            first_order = SupplierOrder.get(suppliers_orders_ids[0])
            supplier = Supplier.get(first_order.supplier_id)
            appstruct['supplier_id'] = supplier.id
            appstruct['name'] = "Facture {}, {}".format(
                supplier.label,
                datetime.date.today(),
            )
        else:
            appstruct['supplier_id'] = None
            appstruct['name'] = "Facture du {}".format(
                datetime.date.today(),
            )

        obj = SupplierInvoice(**appstruct)

        self.dbsession.add(obj)

        for order_id in suppliers_orders_ids:
            order = SupplierOrder.get(order_id)
            order.supplier_invoice = obj
            self.dbsession.merge(order)

            obj.import_lines_from_order(order)

        self.dbsession.flush()

        msg = "La facture a été créée, les lignes ont été copiées depuis "
        if len(suppliers_orders_ids) < 2:
            msg += "la commande fournisseur"
        else:
            msg += "les commandes fournisseurs."
        self.request.session.flash(msg)

        edit_url = self.request.route_path(
            '/suppliers_invoices/{id}',
            id=obj.id,
        )
        return HTTPFound(edit_url)


class SupplierInvoiceEditView(BaseView):
    """
    Can act as edit view or readonly view (eg: waiting for validation).
    """
    def context_url(self):
        return self.request.route_path(
            '/api/v1/suppliers_invoices/{id}',
            id=self.request.context.id
        )

    def form_config_url(self):
        return self.request.route_path(
            '/api/v1/suppliers_invoices/{id}',
            id=self.request.context.id,
            _query={'form_config': '1'}
        )

    def __call__(self):
        populate_actionmenu(self.request)
        supplier_invoice_resources.need()
        return dict(
            context=self.context,
            title=self.context.name,
            context_url=self.context_url(),
            form_config_url=self.form_config_url(),
        )


class SupplierInvoiceListTools(SupplierDocListTools):
    model_class = SupplierInvoice


def stream_supplier_invoice_actions(request, supplier_invoice):
        yield Link(
            request.route_path(
                "/suppliers_invoices/{id}",
                id=supplier_invoice.id,
            ),
            u"Voir/Éditer",
            icon="pen",
            css="icon"
        )
        delete_allowed = request.has_permission(
                'delete.supplier_invoice',
                supplier_invoice,
        )
        if delete_allowed:
            yield POSTButton(
                request.route_path(
                    "/suppliers_invoices/{id}",
                    id=supplier_invoice.id,
                    _query=dict(action="delete"),
                ),
                "Supprimer",
                title="Supprimer définitivement cette facture ?",
                icon="trash-alt",
                css="negative",
                confirm="Êtes-vous sûr de vouloir supprimer cette facture ?"
            )


class BaseSupplierInvoiceListView(
        SupplierInvoiceListTools,
        BaseListView,
):
    title = 'Liste des factures fournisseurs'
    add_template_vars = ['title', 'stream_actions']

    def stream_actions(self, supplier_invoice):
        return stream_supplier_invoice_actions(self.request, supplier_invoice)


class CompanySupplierInvoiceListView(BaseSupplierInvoiceListView):
    """
    Company-scoped list of SupplierOrder
    """
    schema = get_supplier_invoice_list_schema(is_global=False)

    def query(self):
        company = self.request.context
        query = SupplierInvoice.query()
        return query.filter_by(company_id=company.id)


class AdminSupplierInvoiceListView(BaseSupplierInvoiceListView):
    """
    Global list of SupplierOrder from all companies
    """
    is_admin_view = True
    add_template_vars = BaseSupplierInvoiceListView.add_template_vars + [
             'is_admin_view',
    ]

    schema = get_supplier_invoice_list_schema(is_global=True)

    def query(self):
        return SupplierInvoice.query()


class SupplierInvoiceDeleteView(DeleteView):
    delete_msg = u"La facture fournisseur a bien été supprimée"

    def redirect(self):
        return HTTPFound(
            self.request.route_path(
                '/company/{id}/suppliers_invoices',
                id=self.context.company.id
            )
        )


class SupplierPaymentView(BaseFormView):
    """
    Called for setting a payment on a SupplierInvoice
    """
    schema = SupplierPaymentSchema()
    title = "Saisie d'un paiement"

    def before(self, form):
        populate_actionmenu(self.request)

    def redirect(self, come_from):
        if come_from:
            return HTTPFound(come_from)
        else:
            return HTTPFound(
                self.request.route_path(
                    "/suppliers_invoices/{id}", id=self.request.context.id
                )
            )

    def submit_success(self, appstruct):
        """
        Create the payment
        """
        logger.debug("+ Submitting a supplier payment")
        logger.debug(appstruct)
        come_from = appstruct.pop('come_from', None)
        force_resulted = appstruct.pop('resulted', None)
        self.context.record_payment(
            SupplierPayment(**appstruct),
            force_resulted=force_resulted,
        )
        self.dbsession.merge(self.context)
        self.request.session.flash(u"Le paiement a bien été enregistré")
        # FIXME
        # notify_status_changed(self.request, self.context.paid_status)
        return self.redirect(come_from)


SUPPLIER_INVOICE_DISPATCH_GRID = (
    (
        ('date', 2),
        ('supplier_id', 10),
    ),
    (
        ('name', 12),
    ),
    (
        ('invoice_file', 12),
    ),
    (
        ('total_ht', 6),
        ('total_tva', 6),
    ),
    (
        ('lines', 12),
    ),
)


class SupplierInvoiceDispatchView(BaseFormView):
    """
    Used when an EA receives a global supplier invoice that needs to be split,
    enDi-wise, into several supplier invoices.
    """
    add_template_vars = ('title',)
    title = "Ventiler une facture fournisseur"
    schema = SupplierInvoiceDispatchSchema(
        widget=deform_extensions.GridFormWidget(
            named_grid=SUPPLIER_INVOICE_DISPATCH_GRID
        ),
        title="Réception d'une commande fournisseur",
    )

    def before(self, form):
        dispatch_supplier_invoice_js.need()

    @staticmethod
    def _group_lines_by_company(lines):
        ret = {}
        for line in lines:
            try:
                ret[line['company_id']].append(line)
            except KeyError:
                ret[line['company_id']] = [line]
        return ret

    def submit_success(self, appstruct):
        reference_supplier = Supplier.query().get(appstruct['supplier_id'])
        created_invoices = []

        indexed_lines = self._group_lines_by_company(appstruct['lines'])
        for company_id, lines in indexed_lines.items():
            supplier = Supplier.query().filter_by(
                registration=reference_supplier.registration,
                company_id=company_id,
            ).first()
            if supplier is None:
                # Copy minimal information to avoid data leak
                supplier = Supplier(
                    company_id=company_id,
                    company_name=reference_supplier.company_name,
                    registration=reference_supplier.registration,
                )
                self.dbsession.add(supplier)

            invoice = SupplierInvoice(
                date=appstruct['date'],
                company=Company.get(company_id),
                supplier=supplier,
                name=appstruct.get('name', ''),
            )
            if not invoice.name:
                invoice.name = invoice.get_default_name()

            f = File(
                parent=invoice,
            )
            forms.merge_session_with_post(f, appstruct['invoice_file'])
            self.dbsession.add(f)
            self.request.registry.notify(FileAdded(self.request, f))

            self.dbsession.add(invoice)

            for line in lines:
                new_line = SupplierInvoiceLine(
                    supplier_invoice=invoice,
                    description=line['description'],
                    ht=line['ht'],
                    tva=line['tva'],
                    type_id=line['type_id'],
                )
                invoice.lines.append(new_line)
            created_invoices.append(invoice)

        invoices_descriptions = [
            '{}/{}'.format(invoice.company.name, invoice.name)
            for invoice in created_invoices
        ]

        self.session.flash(
            'Les factures suivantes ont été créées : {}'.format(
                ' '.join(invoices_descriptions)
            )
        )
        return HTTPFound('/suppliers_invoices')


def add_routes(config):
    config.add_route(
        '/suppliers_invoices',
        '/suppliers_invoices',
    )

    config.add_route(
        '/company/{id}/suppliers_invoices',
        '/company/{id}/suppliers_invoices',
        traverse='/companies/{id}',
    )

    config.add_route(
        '/suppliers_invoices/{id}',
        '/suppliers_invoices/{id}',
        traverse='/suppliers_invoices/{id}',
    )
    for action in (
        'delete',
#        'duplicate',
        'addpayment',
        'addfile',
    ):
        config.add_route(
            "/suppliers_invoices/{id}/%s" % action,
            "/suppliers_invoices/{id:\d+}/%s" % action,
            traverse="/suppliers_invoices/{id}",
        )
    config.add_route(
        '/dispatch_supplier_invoice',
        '/dispatch_supplier_invoice',
    )


def add_views(config):
    # Admin views
    config.add_view(
        AdminSupplierInvoiceListView,
        request_method='GET',
        route_name='/suppliers_invoices',
        permission='admin.supplier_invoice',
        renderer="/supply/suppliers_invoices.mako",
    )

    config.add_view(
        SupplierInvoiceAddView,
        route_name='/company/{id}/suppliers_invoices',
        request_param='action=new',
        permission='add.supplier_invoice',
        renderer="base/formpage.mako",
    )

    config.add_view(
        CompanySupplierInvoiceListView,
        route_name='/company/{id}/suppliers_invoices',
        request_method='GET',
        renderer='/supply/suppliers_invoices.mako',
        permission='list.supplier_invoice',
    )
    config.add_view(
        SupplierInvoiceEditView,
        route_name="/suppliers_invoices/{id}",
        renderer="/supply/supplier_invoice.mako",
        permission="view.supplier_invoice",
        layout="opa",
    )

    config.add_view(
        SupplierInvoiceDeleteView,
        route_name="/suppliers_invoices/{id}",
        request_param='action=delete',
        permission="delete.supplier_invoice",
        request_method='POST',
        require_csrf=True,
    )

    config.add_view(
        SupplierPaymentView,
        route_name="/suppliers_invoices/{id}/addpayment",
        permission="add_payment.supplier_invoice",
        renderer="base/formpage.mako",
    )

    # File attachment
    config.add_view(
        FileUploadView,
        route_name="/suppliers_invoices/{id}/addfile",
        renderer='base/formpage.mako',
        permission='add.file',
    )

    config.add_view(
        SupplierInvoiceDispatchView,
        route_name="/dispatch_supplier_invoice",
        permission="admin.supplier_invoice",
        renderer="supply/dispatch_supplier_invoice.mako",
    )


def includeme(config):
    add_routes(config)
    add_views(config)
