# -*- coding: utf-8 -*-

import pkg_resources

from endi.resources import (
    main_group,
)

from endi.utils.menu import (
    Menu,
    MenuItem,
)


class SupplierLayout(object):
    endi_version = pkg_resources.get_distribution('endi').version

    def __init__(self, context, request):
        self.context = context
        main_group.need()

    @property
    def docs_menu(self):
        DocsMenu.set_current(self.context)
        return DocsMenu


# Tabs headers with supplier-related documents
DocsMenu = Menu(name="supplier_docs_menu")


DocsMenu.add(
    MenuItem(
        name="running_orders",
        label=u'Commandes en cours',
        route_name='supplier_running_orders',
        icon=u'file-alt',
        anchor='#subview',
    )
)
DocsMenu.add(
    MenuItem(
        name="invoiced_orders",
        label=u'Commandes facturées',
        route_name='supplier_invoiced_orders',
        icon=u'euro-sign',
        anchor='#subview',
    )
)

DocsMenu.add(
    MenuItem(
        name="invoices",
        label=u'Factures',
        route_name='supplier_invoices',
        icon=u'file-invoice-euro',
        anchor='#subview',
    )
)


def includeme(config):
    config.add_layout(
        SupplierLayout,
        template='endi:templates/supplier/layout.mako',
        name='supplier'
    )
