# -*- coding: utf-8 -*-


def admin_accounting_index_view(request):
    menus = []
    for label, route, title, icon in (
        (u"Retour", "admin_index", "", "arrow-left"),
        (
            u"Configurer les États de Trésorerie",
            "/admin/accounting/treasury_measures",
            u"Les états de trésorerie sont générés depuis les balances "
            u"analytiques déposées dans enDI",
            "euro-circle",
        ),
        (
            u"Configurer les Comptes de résultat",
            "/admin/accounting/income_statement_measures",
            u"Les comptes de résultat sont générés depuis les grands livres "
            u"déposés dans enDI",
            "table",
        ),
    ):
        menus.append(
            dict(label=label, route_name=route, title=title, icon=icon)
        )
    return dict(
        title=u"Configuration du module Fichier de trésorerie",
        menus=menus
    )


def includeme(config):
    config.add_route('/admin/accounting', "/admin/accounting")
    config.add_admin_view(
        admin_accounting_index_view,
        route_name="/admin/accounting"
    )
