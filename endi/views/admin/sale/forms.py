# -*- coding: utf-8 -*-
"""
Configuration générale du module vente:

    Mise en forme des PDFs
    Unité de prestation
"""
import logging

from endi.models.task import (
    WorkUnit,
    PaymentConditions,
)
from endi.models.payments import (
    PaymentMode,
)

from endi.views.admin.tools import (
    get_model_admin_view,
)
from endi.views.admin.sale import (
    SaleIndexView,
    SALE_URL,
)

logger = logging.getLogger(__name__)


BaseWorkUnitAdminView = get_model_admin_view(WorkUnit, r_path=SALE_URL)


class WorkUnitAdminView(BaseWorkUnitAdminView):
    disable = False


BasePaymentModeAdminView = get_model_admin_view(PaymentMode, r_path=SALE_URL)


class PaymentModeAdminView(BasePaymentModeAdminView):
    disable = False


PaymentConditionsAdminView = get_model_admin_view(
    PaymentConditions,
    r_path=SALE_URL,
)


def includeme(config):
    for view in (
        WorkUnitAdminView,
        PaymentModeAdminView, PaymentConditionsAdminView
    ):
        config.add_route(view.route_name, view.route_name)
        config.add_admin_view(view, parent=SaleIndexView)
