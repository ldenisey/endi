# -*- coding: utf-8 -*-


def includeme(config):
    config.include('.routes')
    config.include('.layout')
    config.include('.business')
    config.include('.estimation')
    config.include('.invoice')
    config.include('.files')
