# -*- coding: utf-8 -*-
import logging

from endi.models.project.business import Business
from endi.utils.menu import (
    MenuItem,
    Menu,
)
from endi.default_layouts import DefaultLayout
from endi.views.business.routes import (
    BUSINESS_ITEM_ROUTE,
    BUSINESS_ITEM_OVERVIEW_ROUTE,
    # BUSINESS_ITEM_ESTIMATION_ROUTE,
    BUSINESS_ITEM_INVOICE_ROUTE,
    BUSINESS_ITEM_FILE_ROUTE,
)
from endi.views.training.routes import (
    BUSINESS_BPF_DATA_FORM_URL,
    BUSINESS_BPF_DATA_LIST_URL,
)


logger = logging.getLogger(__name__)


BusinessMenu = Menu(name="businessmenu")
BusinessMenu.add(
    MenuItem(
        name='overview',
        label=u"Vue générale",
        route_name=BUSINESS_ITEM_OVERVIEW_ROUTE,
        icon="info-circle",
        perm="view.business",
    )
)
# BusinessMenu.add(
#     MenuItem(
#         name='business_estimations',
#         label=u"Devis",
#         route_name=BUSINESS_ITEM_ESTIMATION_ROUTE,
#         icon=u"file-list",
#         perm="list.estimations",
#     )
# )
BusinessMenu.add(
    MenuItem(
        name='business_invoices',
        label=u"Factures",
        route_name=BUSINESS_ITEM_INVOICE_ROUTE,
        icon=u"file-invoice-euro",
        perm="list.invoices",
    )
)
BusinessMenu.add(
    MenuItem(
        name='business_files',
        label=u"Fichiers attachés",
        route_name=BUSINESS_ITEM_FILE_ROUTE,
        icon=u"paperclip",
    )
)

BusinessMenu.add(
    MenuItem(
        name='bpf_data',
        label=u"Données BPF",
        route_name=BUSINESS_BPF_DATA_LIST_URL,
        other_route_name=BUSINESS_BPF_DATA_FORM_URL,
        perm="edit.bpf",
        icon=u"chart-pie",
    )
)


class BusinessLayout(DefaultLayout):
    """
    Layout for business related pages

    Provide the main page structure for project view
    """

    def __init__(self, context, request):
        DefaultLayout.__init__(self, context, request)

        if isinstance(context, Business):
            self.current_business_object = context
        elif hasattr(context, "business"):
            self.current_business_object = context.business
        else:
            raise Exception(
                u"Can't retrieve the current business used in the "
                u"business layout, context is : %s" % context
            )

    @property
    def edit_url(self):
        return self.request.route_path(
            BUSINESS_ITEM_ROUTE,
            id=self.current_business_object.id,
            _query={'action': 'edit'}
        )

    @property
    def close_url(self):
        return self.request.route_path(
            BUSINESS_ITEM_ROUTE,
            id=self.current_business_object.id,
            _query={'action': 'close'}
        )

    @property
    def businessmenu(self):
        BusinessMenu.set_current(self.current_business_object)
        return BusinessMenu


def includeme(config):
    config.add_layout(
        BusinessLayout,
        template="endi:templates/business/layout.mako",
        name='business',
    )
