# -*- coding: utf-8 -*-
"""

    Panels used for task rendering

"""

from sqla_inspect.py3o import SqlaContext
from endi.models.company import Company
from endi.models.task import Invoice

from endi.utils.strings import major_status
from endi.views.render_api import status_icon


CompanySerializer = SqlaContext(Company)


def task_panel(context, request, task=None, bulk=False):
    """
        Task panel
    """
    if task is None:
        task = context

    tvas = task.get_tvas()
    # Si on a plusieurs TVA dans le document, cela affecte l'affichage
    multiple_tvas = len([val for val in tvas if val]) > 1

    tmpl_context = CompanySerializer.compile_obj(task.project.company)

    # Calcul des nombres de
    if task.display_units:
        column_count = 5
        first_column_colspan = 4
    else:
        column_count = 2
        first_column_colspan = 1

    if multiple_tvas:
        column_count += 1

    if task.display_ttc:
        column_count += 1

    if isinstance(task, Invoice) and \
            task.invoicing_mode == task.PROGRESS_MODE:
        show_progress_invoicing = True
        column_count += 1
        first_column_colspan += 1
    else:
        show_progress_invoicing = False

    return dict(
        task=task,
        groups=task.get_groups(),
        project=task.project,
        company=task.project.company,
        multiple_tvas=multiple_tvas,
        tvas=tvas,
        config=request.config,
        bulk=bulk,
        mention_tmpl_context=tmpl_context,
        first_column_colspan=first_column_colspan,
        column_count=column_count,
        show_progress_invoicing=show_progress_invoicing,
    )


def task_line_group_panel(
    context,
    request,
    task,
    group,
    display_tvas_column,
    column_count,
    first_column_colspan,
    show_progress_invoicing,
):
    """
    A panel representing a TaskLineGroup
    """
    display_subtotal = False
    if len(task.get_groups()) > 1:
        display_subtotal = True

    return dict(
        task=task,
        group=group,
        display_subtotal=display_subtotal,
        display_units=task.display_units,
        display_tvas_column=display_tvas_column,
        display_ttc=task.display_ttc,
        column_count=column_count,
        first_column_colspan=first_column_colspan,
        show_progress_invoicing=show_progress_invoicing,
    )


def task_line_panel(
    context,
    request,
    task,
    line,
    display_tvas_column,
    column_count,
    first_column_colspan,
    show_progress_invoicing,
):
    """
    A panel representing a single TaskLine

    :param obj task: The current task to be rendered
    :param obj line: A taskline
    """
    percentage = 0
    if show_progress_invoicing:
        from endi.models.progress_invoicing import ProgressInvoicingLine
        percentage = ProgressInvoicingLine.find_percentage(line.id)
        if percentage is None:
            percentage = 0

    return dict(
        task=task,
        line=line,
        display_units=task.display_units,
        display_tvas_column=display_tvas_column,
        display_ttc=task.display_ttc,
        column_count=column_count,
        first_column_colspan=first_column_colspan,
        show_progress_invoicing=show_progress_invoicing,
        progress_invoicing_percentage=percentage,
    )


STATUS_LABELS = {
    'draft': u"Brouillon",
    "wait": u"En attente de validation",
    "invalid": {
        "estimation": u"Invalidé",
        "invoice": u"Invalidée",
        "cancelinvoice": u"Invalidé",
        "expensesheet": u"Invalidée",
        "supplier_order": u"Invalidée",
    },
    "valid": {
        "estimation": u"En cours",
        "invoice": u"En attente de paiement",
        "cancelinvoice": u"Soldé",
        "expensesheet": u"Validée",
        "supplier_order": u"Validée",
    },
    "aborted": u"Sans suite",
    'sent': u"Envoyé",
    "signed": u"Signé par le client",
    "geninv": u"Factures générées",
    "paid": u"Payée partiellement",
    "resulted": u"Soldée",
    "justified": u"Justificatifs reçus",
}


def task_title_panel(context, request, title):
    """
    Panel returning a label for the given context's status
    """
    # FIXME: factorize properly into render_api and common panels : this is
    # used for other stuff than tasks.
    # See render_api.STATUS_CSS_CLASS, among others

    status = major_status(context)
    print("The major status is : %s" % status)
    status_label = STATUS_LABELS.get(status)
    if isinstance(status_label, dict):
        status_label = status_label[context.type_]

    css = u'status status-%s' % context.status
    if hasattr(context, 'paid_status'):
        css += u' paid-status-%s' % context.paid_status
        if hasattr(context, 'is_tolate'):
            css += u' tolate-%s' % context.is_tolate()
        elif hasattr(context, 'justified'):
            css += u' justified-%s' % context.justified
    elif hasattr(context, 'signed_status'):
        css += u' signed-status-%s geninv-%s' % (
            context.signed_status,
            context.geninv,
        )
    else:  # cancelinvoice
        if status == 'valid':
            css += ' paid-status-resulted'

    return dict(
        title=title,
        item=context,
        css=css,
        status_label=status_label
    )


def includeme(config):
    """
        Pyramid's inclusion mechanism
    """
    for document_type in ('estimation', 'invoice', 'cancelinvoice'):
        panel_name = "{0}_html".format(document_type)
        template = "panels/task/{0}.mako".format(document_type)
        config.add_panel(task_panel, panel_name, renderer=template)

    config.add_panel(
        task_line_group_panel,
        'task_line_group_html',
        renderer="panels/task/task_line_group.mako"
    )

    config.add_panel(
        task_line_panel,
        'task_line_html',
        renderer="panels/task/task_line.mako"
    )

    config.add_panel(
        task_title_panel,
        "task_title_panel",
        renderer="endi:templates/panels/task/title.mako",
    )
