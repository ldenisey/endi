# -*- coding: utf-8 -*-
from endi.utils.widgets import (
    Link,
    POSTButton,
)
from endi.views.project.routes import (
    PROJECT_ITEM_INVOICE_ROUTE,
    PROJECT_ITEM_ESTIMATION_ROUTE,
)


def _stream_actions(request, item):
    """
    Return actions that will be rendered in a dropdown
    """
    yield Link(
        request.route_path("/%ss/{id}" % item.type_, id=item.id),
        u"Voir / Modifier",
        icon="pen",
        css="icon"
    )
    yield Link(
        request.route_path("/%ss/{id}.pdf" % item.type_, id=item.id),
        u"PDF",
        title=u"Enregistrer le PDF",
        icon="file-pdf",
        css="icon"
    )
    if request.has_permission('duplicate.%s' % item.type_, item):
        yield Link(
            request.route_path("/%ss/{id}/duplicate" % item.type_, id=item.id),
            u"Dupliquer",
            icon="copy",
            css="icon"
        )

    if request.has_permission('delete.%s' % item.type_, item):
        yield POSTButton(
            request.route_path("/%ss/{id}/delete" % item.type_, id=item.id),
            u"Supprimer",
            confirm=u"Êtes-vous sûr de vouloir supprimer ce document ?",
            icon="trash-alt",
            css="icon negative"
        )

    for phase in request.context.phases:
        if phase.id != item.phase_id:
            yield POSTButton(
                request.route_path(
                    "/%ss/{id}/move" % item.type_, id=item.id,
                    _query={'phase': phase.id}
                ),
                u"Déplacer vers le sous-dossier %s" % phase.name,
                icon="arrows-alt-v",
                css="icon"
            )


def phase_estimations_panel(context, request, phase, estimations):
    """
    Phase estimation list panel
    """
    _query = {'action': 'add'}
    if phase is not None:
        _query['phase'] = phase.id

    add_url = request.route_path(
        PROJECT_ITEM_ESTIMATION_ROUTE,
        id=context.id,
        _query=_query
    )

    return dict(
        add_url=add_url,
        estimations=estimations,
        stream_actions=_stream_actions,
    )


def phase_invoices_panel(context, request, phase, invoices):
    """
    Phase invoice list panel
    """
    _query = {'action': 'add'}
    if phase is not None:
        _query['phase'] = phase.id

    add_url = request.route_path(
        PROJECT_ITEM_INVOICE_ROUTE,
        id=context.id,
        _query=_query
    )

    return dict(
        add_url=add_url,
        invoices=invoices,
        stream_actions=_stream_actions,
    )


def includeme(config):
    config.add_panel(
        phase_estimations_panel,
        'phase_estimations',
        renderer="endi:templates/panels/project/phase_estimations.mako"
    )
    config.add_panel(
        phase_invoices_panel,
        'phase_invoices',
        renderer="endi:templates/panels/project/phase_invoices.mako"
    )
