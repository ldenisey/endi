Guide de style
==============


Gestion des fichiers de style
-----------------------------

Les feuilles de style sont basées sur des sources SASS situées dans le répertoire css_sources.
On peut les compiler automatiquement à chaque modification avec la commande ``make css``.
Les fichiers .sass sont structurés en fragments correspondant aux diverses parties structurelles du site (mise en forme générale, menu, tableaux, etc… ).


Règles générales
----------------

Quand on signale l’absence de contenu (« Pas de produit », « Aucun contenu n’a été ajouté », « Pas de devis »…), mettre le texte correspondant en italique au moyen d’un élément ``<em>``.


Colonnes
--------

2 à 4 Colonnes 
~~~~~~~~~~~~~~

Pour structurer le contenu d’une ``<div>`` en deux à quatre colonnes, appliquer les classes ``layout`` et ``flex``, et une des classes ``two_cols`` ``three_cols`` ou  ``four_cols``. Cela répartit tous les éléments directement contenus dans cette ``<div>`` en 2, 3 ou 4 colonnes de largeur égale.

Pour les structures en 2 colonnes, 4 classes à ajouter à  ``layout flex two_cols``permettent d’obtenir deux colonnes de largeurs différentes :
- ``third`` colonne de gauche 1/3, colonne de droite 2/3
- ``third_reverse`` colonne de gauche 2/3, colonne de droite 1/3
- ``quarter`` colonne de gauche 1/4, colonne de droite 3/4
- ``quarter_reverse`` colonne de gauche 3/4, colonne de droite 1/4

Rangs divisés en colonnes
~~~~~~~~~~~~~~~~~~~~~~~~~

Une ``<div>`` considérée comme un rang (particulièrement dans les formulaires) est structurée en colonnes en utilisant les classes ``col-md-1`` (1/12ème) à ``col-md-12`` (12/12èmes, 100%) sur chaque élément du rang. 
La gestion responsive de la largeur des colonnes dispense d’utiliser les classes de type ``col-xs`` ou autre, qui n’ont pas d’effet et n’espacent pas les colonnes de formulaires.


Tableaux
--------

Tableaux ``<table>``
~~~~~~~~~~~~~~~~~~~~

- La classe ``top_align_table`` permet d’aligner verticalement le contenu de chaque case du tbody en haut de case. À utiliser pour les tableaux dont certaines cases ont des contenus très longs, pour faciliter la lecture ligne à ligne. Sans cette classe, l’alignement vertical par défaut est centré.
- La classe ``hover_table`` permet d’ajouter une couleur au survol à chaque ligne du tableau. À utiliser quand une ou plusieurs actions peuvent être réalisées en cliquant sur une ligne ou sur le contenu des cases autres que ``col_actions``.
- La classe ``spaced_table`` permet d’ajouter de l’espace au-dessus et au-dessous du tableau.
- La classe ``full_width`` permet de forcer la largeur du tableau à 100% de son contenant.

Rangs ``<tr>``
~~~~~~~~~~~~~~

- La classe ``top_align`` permet d’aligner verticalement le contenu de chaque case du rang en haut de case. Cf. ``top_align_table`` pour l’utilisation.

En-têtes ``<th>``
~~~~~~~~~~~~~~~~~

- utiliser l’attribut scope pour définir si l’en-tête est un en-tête de colonne (``scope="col"``) ou un en-tête de ligne (``scope="row"``)

Cases ``<td>``
~~~~~~~~~~~~~~

- La classe ``top_align`` permet d’aligner verticalement le contenu de la case en haut de case. Cf. ``top_align_table`` pour l’utilisation.

Boutons d’action dans les lignes de tableaux
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Quand au maximum 4 actions faciles à différencier graphiquement sont réalisables sur une ligne, les boutons d’action sont insérés directement dans la colonne ``col_actions`` sous forme de boutons icônes.
- Quand il y a plus d’actions où qu’elles ne sont pas différentiables graphiquement, les boutons sont insérés dans un sous-menu qui s’ouvre au clic sur un bouton (icône ``#dots``)

Pour éviter que les cases d’actions s’élargissent trop quand l’écran est large, ajouter sur le ``<td class="col_actions">`` une classe correspondant au nombre de boutons présents dans la case :

- ``width_one`` pour 1 bouton ( ``<td class="col_actions width_one">``)
- ``width_two`` pour 2 boutons ( ``<td class="col_actions width_two">``)
- ``width_three`` pour 3 boutons ( ``<td class="col_actions width_three">``)
- ``width_four`` pour 4 boutons ( ``<td class="col_actions width_four">``)

Barre de boutons
----------------

Les boutons d’action indépendants ou portant sur tout le contenu de la page sont affichés dans la barre de boutons présente en haut d’écran.

- Les boutons peuvent être séparés en groupes
- Les actions principales sont affichées en haut à gauche
- Les actions secondaires sont affichées en haut à droite


Formulaires
-----------

Les formulaires complexes sont structurés en blocs (``<fieldset>``).
Ces blocs sont séparés graphiquement avec les classes ``separate_block`` (ombre et espacement) et ``border_left_block`` (bordure colorée à gauche du bloc).

Quand un formulaire ne contient qu’un bloc, ne pas le séparer graphiquement (ne pas utiliser ``separate_block`` et ``border_left_block``).
