import AppRouter from 'marionette.approuter';

const Router = AppRouter.extend({
    appRoutes: {
        'index': 'index',
        '': 'index',
        'addproduct': 'addProduct',
        'products/:id': 'editProduct',
        'addwork': "addWork",
        "works/:id": "editWork",
        'adddiscount': "addDiscount",
        "discounts/:id": "editDiscount",
    }
});
export default Router;
