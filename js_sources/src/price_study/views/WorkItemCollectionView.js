/*
 * Module name : WorkItemCollectionView
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import WorkItemView from './WorkItemView.js';
import WorkItemEmptyView from './WorkItemEmptyView.js';

const template = require('./templates/WorkItemCollectionView.mustache');

const WorkItemCollectionView = Mn.CollectionView.extend({
    template: template,
    childView: WorkItemView,
    emptyView: WorkItemEmptyView,
    childViewContainer: "tbody",
    ui: {},
    // Listen to the current's view events
    events: {},
    // Listen to child view events
    childViewEvents: {},
    // Bubble up child view events
    childViewTriggers: {
    },
    initialize(){
        this.config = Radio.channel('config');
    },
    onRender(){
    },
    templateContext(){
        return {};
    }
});
export default WorkItemCollectionView