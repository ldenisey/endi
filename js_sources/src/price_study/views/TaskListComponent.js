/*
 * Module name : TaskListComponent
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

const template = require('./templates/TaskListComponent.mustache');


const TaskListComponent = Mn.View.extend({
    template: template,
    regions: {},
    ui: {},
    // Listen to the current's view events
    events: {},
    // Listen to child view events
    childViewEvents: {},
    // Bubble up child view events
    childViewTriggers: {
    },
    initialize(){
        this.config = Radio.channel('config');
    },
    onRender(){
    },
    templateContext(){
        let tasks = this.getOption('tasks');
        let editable = this.config.request('get:options', 'editable');
        return {tasks: tasks, edit: editable};
    }
});
export default TaskListComponent
