/*
 * File Name :  WorkItemCollection
 */
import OrderableCollection from '../../base/models/OrderableCollection.js';
import Radio from 'backbone.radio';
import WorkItemModel from './WorkItemModel.js';
import {ajax_call} from '../../tools.js';

const WorkItemCollection = OrderableCollection.extend({
    model: WorkItemModel,
    initialize: function(options) {
        WorkItemCollection.__super__.initialize.apply(this, options);
    },
    ht: function(){
        /* Sum the HT amounts for this collection */
        var result = 0;
        this.each(function(model){
            result += model.ht();
        });
        return result;
    },
    tvaParts: function(){
        /*
         * Collect tva amounts by tva  label
         */
        var result = {};
        this.each(function(model){
            var tva_amount = model.tva();
            var tva = model.tva_label();
            if (tva != '-'){
                if (tva in result){
                    tva_amount += result[tva];
                }
                result[tva] = tva_amount;
            }
        });
        return result;
    },
    ttc: function(){
        var result = 0;
        this.each(function(model){
            result += model.ttc();
        });
        return result;
    },
    validate: function(){
        var result = {};
        this.each(function(model){
            var res = model.validate();
            if (res){
                result['items'] = "Des modèles sont incomplets, veuillez vérifier votre saisie";
            }
        });
        return result;
    },
    syncAll(models){
        console.log("WorkItemCollection Syncing all models");
        var promises = [];
        this.each(function(model){
            promises.push(model.fetch());
        });
        var resulting_deferred = $.when(...promises);
        return resulting_deferred;
    },
    refreshAll(models){
        return this.syncAll(models);
    }
});
export default WorkItemCollection;
