/*
 * File Name :  PriceStudyModel
 */
import Bb from 'backbone';
import Radio from 'backbone.radio';

import { formatAmount } from '../../math.js';
import BaseModel from '../../base/models/BaseModel.js';

const PriceStudyModel = BaseModel.extend({
    props: [
        "id",
        "product_name",
        "label",
        "notes",
        "total_ht",
        "total_ttc",
        "tva_parts",
    ],
    validation: {
        label: {required: true, msg: "Ce champ est obligatoire"}
    },
    initialize: function(){
        BaseModel.__super__.initialize.apply(this, arguments);
        this.facade = Radio.channel('facade');
        this.config = Radio.channel('config');
    },
    tva_labels: function(){
        var values = [];
        var this_ = this;
        const tva_options = this.config.request(
            'get:options',
            'tvas'
        );

        _.each(
            this.get('tva_parts'),
            function(item, tva_id){
                let tva = _.findWhere(tva_options, {id: parseInt(tva_id)});
                values.push({'value': formatAmount(item), label: tva.label});
        });
        return values;
    },
    ht_label(){
        return formatAmount(this.get('total_ht'));
    },
    ttc_label(){
        return formatAmount(this.get('total_ttc'));
    }
});
export default PriceStudyModel;
