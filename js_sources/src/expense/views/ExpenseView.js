import Mn from 'backbone.marionette';
import { formatAmount } from '../../math.js';

const tel_template = require('./templates/ExpenseTelView.mustache');
const template = require('./templates/ExpenseView.mustache');

require("jquery-ui/ui/effects/effect-highlight");

const ExpenseView = Mn.View.extend({
    tagName: 'tr',
    ui: {
        edit: 'button.edit',
        delete: 'button.delete',
        duplicate: 'button.duplicate',
        bookmark: 'button.bookmark',
    },
    triggers: {
        'click @ui.edit': 'edit',
        'click @ui.delete': 'delete',
        'click @ui.duplicate': 'duplicate',
        'click @ui.bookmark': 'bookmark',
    },
    modelEvents: {
        'sync': 'render'
    },
    getTemplate(){
        if (this.model.isTelType()){
            return tel_template;
        } else {
            return template;
        }
    },
    highlightBookMark(){
        this.getUI('bookmark').effect("highlight", {color: "#ceff99"}, "slow");
    },
    templateContext(){
        var total = this.model.total();
        var typelabel = this.model.getTypeLabel();
        var is_achat = false;
        if(this.model.get('category')==2) is_achat=true;
        return {
            edit: this.getOption('edit'),
            customer: this.model.get('customer_label'),
            is_achat: is_achat,
            typelabel: typelabel,
            total: formatAmount(total),
            ht_label: formatAmount(this.model.get('ht')),
            tva_label: formatAmount(this.model.get('tva')),
        };
    }
});
export default ExpenseView;
