import Mn from 'backbone.marionette';
import { formatAmount } from '../../math.js';

const ExpenseKmView = Mn.View.extend({
    tagName: 'tr',
    template: require('./templates/ExpenseKmView.mustache'),
    modelEvents: {
        'sync': 'render'
    },
    ui: {
        edit: 'button.edit',
        delete: 'button.delete',
        duplicate: 'button.duplicate',
    },
    triggers: {
        'click @ui.edit': 'edit',
        'click @ui.delete': 'delete',
        'click @ui.duplicate': 'duplicate',
    },
    templateContext(){
        var total = this.model.total();
        var typelabel = this.model.getTypeLabel();
        var is_achat = false;
        if(this.model.get('category')==2) is_achat=true;
        return {
            edit: this.getOption('edit'),
            customer: this.model.get('customer_label'),
            is_achat: is_achat,
            typelabel:typelabel,
            total:formatAmount(total),
        };
    }
});
export default ExpenseKmView;
