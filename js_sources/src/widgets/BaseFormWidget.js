/*
*
*/
import Mn from 'backbone.marionette';
import { getOpt } from '../tools.js';

const BaseFormWidget = Mn.View.extend({
    initialize(){
      this.cid = _.uniqueId();
    },
    getTagId: function(){
        /*
         * Return an id for the current textarea
         */
        var field_name = this.getOption('field_name');
        return field_name + "-" + this.cid;
    },
    getCommonContext(){
        let label = getOpt(this, 'title', '') || getOpt(this, 'label', '');
        let ariaLabel = getOpt(this, 'ariaLabel', '') || label;
        let placeholder = getOpt(this, 'placeholder', ariaLabel);
        let description = getOpt(this, 'description', '');
        let editable = getOpt(this, 'editable', true);
        let required = getOpt(this, 'required', false);
        return {
            ariaLabel: ariaLabel,
            label: label,
            title: label, // For retro-compatibility
            description: description,
            placeholder: placeholder,
            field_name: this.getOption('field_name'),
            tagId: this.getTagId(),
            editable: editable,
            required: required
        };
    }
});
export default BaseFormWidget;
