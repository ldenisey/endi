import BaseFormWidget from './BaseFormWidget.js';
import { getOpt } from "../tools.js";

import setupTinyMce from '../tinymce.js';

var template = require('./templates/TextAreaWidget.mustache');

const TextAreaWidget = BaseFormWidget.extend({
    /*
     * A textarea widget
     *
     * Support following options :
     *
     *   :param str title:
     *   :param str description:
     *   :param str field_name:
     *   :param str value:
     *   :param int rows:
     *   :param bool editable:
     *   :param str placeholder:
     *   :param bool tinymce: Richtext widget ?
     *
     * Triggers two type of events
     *
     *   change : on KeyUp
     *   finish : when leaving the field
     */
    tagName: 'div',
    className: 'form-group',
    template: template,
    ui:{
        textarea: 'textarea'
    },
    events: {
        'keyup @ui.textarea': 'onKeyUp',
        'blur @ui.textarea': 'onBlur'
    },
    onKeyUp: function(){
        this.triggerMethod(
            'change',
            this.getOption('field_name'),
            this.getUI('textarea').val(),
        );
    },
    onBlur: function(){
        this.triggerMethod(
            'finish',
            this.getOption('field_name'),
            this.getUI('textarea').val(),
        );
    },

    templateContext: function(){
        let ctx = this.getCommonContext();
        let more_ctx = {
            value: getOpt(this, 'value', ''),
            rows: getOpt(this, 'rows', 2),
        }
        
        return Object.assign(ctx, more_ctx);
    },
    onAttach: function(){
        var tiny = getOpt(this, 'tinymce', false);
        if (tiny){
            var onblur = this.onBlur.bind(this);
            var onkeyup = this.onKeyUp.bind(this);
            setupTinyMce(
                {
                    selector: "#" + this.getTagId(),
                    init_instance_callback: function (editor) {
                        editor.on('blur', onblur);
                        editor.on('keyup', onkeyup);
                        editor.on('change', function () {
                            tinymce.triggerSave();
                        });
                    }
                }
            );
        }
    }
});
export default TextAreaWidget;
