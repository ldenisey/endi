import Radio from 'backbone.radio';

import BaseModel from "../../base/models/BaseModel.js";
import DuplicableMixin from '../../base/models/DuplicableMixin.js';

const SupplierInvoiceModel =  BaseModel.extend(DuplicableMixin).extend({
    props: [
        'id',
        'date',
        'name',
        'supplier_orders',
        'orders_total',
        'orders_cae_total',
        'orders_worker_total',
        'orders_total_ht',
        'orders_total_tva',
        'cae_percentage',
        'payments',
        'supplier_name',
    ],
    defaults: {
        supplier_orders: []
    },
    validation: {
        date: {
            required: true,
        },
        name: {
            required: true,
        },
    },
    initialize(){
        SupplierInvoiceModel.__super__.initialize.apply(this, arguments);
        this.on('change:supplier_orders', this.ensureTypesIsList, this);
    },
    ensureTypesIsList(){
        let orders = this.get('supplier_orders');
        if (!_.isArray(orders)){
            this.attributes['supplier_orders'] = [orders];
        }
    },

});
export default SupplierInvoiceModel;
