import Mn from 'backbone.marionette';
import SupplierInvoiceLineCollectionView from './SupplierInvoiceLineCollectionView.js';
import Radio from 'backbone.radio';
import { formatAmount } from '../../math.js';


const SupplierInvoiceLineTableView = Mn.View.extend({
    template: require('./templates/SupplierInvoiceLineTableView.mustache'),
    regions: {
       lines: {
           el: 'tbody',
           replaceElement: true
       }
    },
    ui:{
        add_btn: 'button.add',
        total_ht: '.total_ht',
        total_tva: '.total_tva',
        total_ttc: '.total_ttc'
    },
    triggers: {
        'click @ui.add_btn': 'line:add',
    },
    childViewTriggers: {
        'line:edit': 'line:edit',
        'line:delete': 'line:delete',
        'line:duplicate': 'line:duplicate',
    },
    initialize(){
        var channel = Radio.channel('facade');
        this.totalmodel = channel.request('get:totalmodel');

        this.listenTo(
            channel,
            'change:lines',
            this.showTotals.bind(this)
        );
    },
    showTotals(){
        this.getUI("total_ht").html(
            formatAmount(this.totalmodel.get('ht'))
        );
        this.getUI("total_tva").html(
            formatAmount(this.totalmodel.get('tva'))
        );
        this.getUI("total_ttc").html(
            formatAmount(this.totalmodel.get('ttc'))
        );
    },
    templateContext(){
        return {
            edit: this.getOption('edit'),
        };
    },
    onRender(){
        var view = new SupplierInvoiceLineCollectionView(
            {
                collection: this.collection,
                edit: this.getOption('edit'),
            }
        );
        this.showChildView('lines', view);
    },
    onAttach(){
        this.showTotals();
    }
});
export default SupplierInvoiceLineTableView;
