import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import { formatPrice, formatAmount } from '../../math.js';
import CAEPartView from './CAEPartView.js';


const TotalView = Mn.View.extend({
    tagName: 'div',
    template: require('./templates/TotalView.mustache'),
    modelEvents: {
        'change:ttc': 'render',
        'change:ht': 'render',
        'change:tva': 'render',
        'change:ttc_cae': 'render',
        'change:ttc_worker': 'render',
    },
    regions: {
        caePartRegion: '.cae-part-region',
    },
   initialize(){
       this.facade = Radio.channel('facade');
       this.supplierInvoice = this.facade.request('get:model');
   },
    showCAEPartView(){
       var view = new CAEPartView();
       this.showChildView('caePartRegion', view);
    },
    onRender(){
        this.showCAEPartView();
    },
    templateContext(){
        let orders_total = this.supplierInvoice.get('orders_total');
        let invoice_total = this.model.get('ttc');
        let totals_mismatch =  orders_total != invoice_total;

        return {
            ht: formatAmount(this.model.get('ht')),
            tva: formatAmount(this.model.get('tva')),
            ttc: formatAmount(this.model.get('ttc')),
            ttc_cae: formatAmount(this.model.get('ttc_cae')),
            ttc_worker: formatAmount(this.model.get('ttc_worker')),
            orders_ttc_worker: formatAmount(this.supplierInvoice.get('orders_worker_total')),
            orders_ttc_cae: formatAmount(this.supplierInvoice.get('orders_cae_total')),
            orders_ht: formatAmount(this.supplierInvoice.get('orders_total_ht')),
            orders_tva: formatAmount(this.supplierInvoice.get('orders_total_tva')),
            orders_ttc: formatAmount(this.supplierInvoice.get('orders_total')),
            cae_percentage: this.supplierInvoice.get('cae_percentage'),
            worker_percentage: 100 - this.supplierInvoice.get('cae_percentage'),

            totals_mismatch: totals_mismatch,
            payments: this.supplierInvoice.get('payments'),
        };
    }
});
export default TotalView;
