import _ from 'underscore';
import BaseModel from "../../base/models/BaseModel.js";
import Radio from 'backbone.radio';


const NotesModel = BaseModel.extend({
    props: [
        'id',
        'notes',
    ],
});
export default NotesModel;
