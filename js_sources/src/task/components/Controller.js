import Radio from 'backbone.radio';
import Mn from 'backbone.marionette';

const Controller = Mn.Object.extend({
    initialize(options){
        this.facade = Radio.channel('facade');
        console.log("Controller.initialize");
        this.rootView = options['rootView'];
    },
    index: function(){
        this.rootView.render();
    },
    status: function(status){
        this.rootView.showBox(status);
    }
});
export default Controller
