import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import StatusHistoryCollectionView from './StatusHistoryCollectionView.js';


const StatusHistoryView = Mn.View.extend({
    template: require('./templates/StatusHistoryView.mustache'),
    regions: {
        comments: '.comments'
    },
    onRender(){
        var collection = this.getOption('collection');
        this.showChildView(
            "comments",
            new StatusHistoryCollectionView({collection: collection})
        );
    }
});
export default StatusHistoryView;
