import Mn from 'backbone.marionette';

import {displayServerSuccess, displayServerError} from '../../../backbone-tools.js';
import { getOpt } from '../../../tools.js';
import ErrorView from '../../../base/views/ErrorView.js';

import TaskGroupModel from '../../models/TaskGroupModel.js';
import TaskGroupCollectionView from './TaskGroupCollectionView.js';
import {default as ProgressInvoicingTaskGroupCollectionView} from "./progress_invoicing/TaskGroupCollectionView.js";
import TaskGroupFormView from './TaskGroupFormView.js';
import {default as ProgressInvoicingTaskGroupFormView} from './progress_invoicing/TaskGroupFormView.js';

import DisplayUnitsView from './DisplayUnitsView.js';
import DisplayTTCView from './DisplayTTCView.js';

import Radio from "backbone.radio";

const LinesComponent = Mn.View.extend({
    /*
     * wrapper for TaskGroup and TaskLine collections
     * It takes the following parameters

        collection

           TaskLineGroups

        edit

            Can we edit groups and lines

     *
     */

    template: require('./templates/LinesComponent.mustache'),
    tagName: 'div',
    className: 'form-section',
    regions: {
        errors: '.group-errors',
        container: '.group-container',
        modalRegion: ".group-modalregion",
        display_units_container: '.display-units-container',
        display_ttc_container: '.display-ttc-container',
    },
    ui: {
        add_button: 'button.add',
    },
    triggers: {
        "click @ui.add_button": "group:add"
    },
    childViewEvents: {
        'group:edit': 'onGroupEdit',
        'group:delete': 'onGroupDelete',
        'catalog:insert': 'onCatalogInsert',
    },
    collectionEvents: {
        'change': 'hideErrors'
    },
    initialize: function(options){
        this.collection = options['collection'];
        this.listenTo(this.collection, 'validated:invalid', this.showErrors);
        this.listenTo(this.collection, 'validated:valid', this.hideErrors.bind(this));
        this.edit = options['edit'];
    },
    showErrors(model, errors){
        this.detachChildView('errors');
        this.showChildView('errors', new ErrorView({errors: errors}));
        this.$el.addClass('error');
    },
    hideErrors(model){
        this.detachChildView('errors');
        this.$el.removeClass('error');
    },
    onDeleteSuccess: function(){
        displayServerSuccess("Vos données ont bien été supprimées");
    },
    onDeleteError: function(){
        displayServerError("Une erreur a été rencontrée lors de la " +
                           "suppression de cet élément");
    },
    onGroupDelete: function(childView){
        var result = window.confirm("Êtes-vous sûr de vouloir supprimer ce produit composé ?");
        if (result){
            childView.model.destroy(
                {
                    success: this.onDeleteSuccess,
                    error: this.onDeleteError
                }
            );
        }
    },
    onGroupEdit: function(childView){
        this.showTaskGroupForm(childView.model, "Modifier ce produit composé");
    },
    onGroupAdd: function(){
        var model = new TaskGroupModel({
            order: this.collection.getMaxOrder() + 1
        });
        this.showTaskGroupForm(model, "Ajouter un produit composé");
    },
    onEditGroup: function(childView){
        var model = childView.model;
        this.showTaskGroupForm(model, "Modifier ce produit composé");
    },
    showTaskGroupForm: function(model, title){
        let form;
        if (this.getOption('section')['mode'] == 'progress'){
            form = new ProgressInvoicingTaskGroupFormView(
                {
                    model: model,
                    title: title,
                    destCollection: this.collection
                }
            );
        } else {
            form = new TaskGroupFormView(
                {
                    model: model,
                    title: title,
                    destCollection: this.collection
                }
            );
        }
        this.showChildView('modalRegion', form);
    },
    onCatalogInsert: function(model){
        let ids = [model.get('id')]
        this.collection.load_from_catalog(ids);
        this.getChildView('modalRegion').triggerMethod('modal:close')
    },
    onChildviewDestroyModal: function() {
        this.detachChildView('modalRegion');
    	this.getRegion('modalRegion').empty();
  	},
    onRender: function(){
        let view;
        if (this.getOption('section')['mode'] == 'progress'){
            view = new ProgressInvoicingTaskGroupCollectionView(
                {collection: this.collection}
            );
        }else {
            if (this.edit){
                view = new DisplayUnitsView({model: this.model})
                this.showChildView('display_units_container', view);
                view = new DisplayTTCView({model: this.model})
                this.showChildView('display_ttc_container', view);
            }
            view = new TaskGroupCollectionView(
                {collection: this.collection, edit: this.edit}
            )
        }

        this.showChildView('container', view);
    },
    templateContext(){
        return {
            show_add_button: this.edit && this.getOption('section')['mode'] == 'classic',
        };
    }
});
export default LinesComponent;
