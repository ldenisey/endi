import Mn from 'backbone.marionette';
import { ajax_call, getOpt } from '../../../tools.js';
import {strToFloat} from '../../../math.js';
import InputWidget from '../../../widgets/InputWidget.js';
import SelectWidget from '../../../widgets/SelectWidget.js';
import TextAreaWidget from '../../../widgets/TextAreaWidget.js';
import ModalFormBehavior from '../../../base/behaviors/ModalFormBehavior.js';
import CatalogComponent from '../../../common/views/CatalogComponent.js';
import TvaProductFormMixin from '../../../base/views/TvaProductFormMixin.js';
import LoadingWidget from '../../../widgets/LoadingWidget.js';
import Radio from 'backbone.radio';

var template = require('./templates/TaskLineFormView.mustache');

const TaskLineFormView = Mn.View.extend(TvaProductFormMixin).extend({
    id: 'task_line_form',
    template: template,
    behaviors: [ModalFormBehavior],
    regions: {
        'order': '.order',
        'description': '.description',
        'cost': '.cost',
        'quantity': '.quantity',
        'unity': '.unity',
        'tva': '.tva',
        'product_id': '.product_id',
        'catalogContainer': '#catalog-container'
    },
    ui: {
        main_tab: 'ul.nav-tabs li:first a'
    },
    childViewEvents: {
        'catalog:insert': 'onCatalogEdit'
    },
    // Bubble up child view events
    //
    childViewTriggers: {
        'change': 'data:modified',
        'finish': 'data:modified'
    },
    modelEvents: {
        'set:product': 'refreshForm',
        'change:tva': 'refreshTvaProductSelect',
    },
    initialize(options) {
        var channel = Radio.channel('config');
        this.workunit_options = channel.request(
            'get:options',
            'workunits'
        );
        this.tva_options = channel.request(
            'get:options',
            'tvas'
        );
        this.product_options = channel.request(
            'get:options',
            'products',
        );
        this.all_product_options = channel.request(
            'get:options',
            'products'
        );
        this.compute_mode = channel.request('get:options', 'compute_mode');
    },
    onCatalogEdit: function(product_datas){
        this.model.loadProduct(product_datas);
    },
    isAddView: function(){
        return !getOpt(this, 'edit', false);
    },
    templateContext: function(){
        return {
            title: this.getOption('title'),
            add: this.isAddView()
        };
    },
    refreshForm: function(){
        this.showChildView(
            'order',
            new InputWidget({
                value: this.model.get('order'),
                field_name:'order',
                type: 'hidden',
            })
        );
        this.showChildView(
            'description',
            new TextAreaWidget({
                value: this.model.get('description'),
                title: "Intitulé des postes",
                field_name: "description",
                tinymce: true,
                cid: this.model.cid
            })
        );

        let is_ttc_mode = this.compute_mode == 'ttc';
        this.showChildView(
            'cost',
            new InputWidget(
                {
                    value: this.model.get('cost'),
                    title: "Prix unitaire " + (is_ttc_mode ? "TTC" : "HT"),
                    field_name: "cost",
                    addon: "€"
                }
            )
        );
        this.showChildView(
            'quantity',
            new InputWidget(
                {
                    value: this.model.get('quantity'),
                    title: "Quantité",
                    field_name: "quantity"
                }
            )
        );
        this.showChildView(
            'unity',
            new SelectWidget(
                {
                    options: this.workunit_options,
                    title: "Unité",
                    value: this.model.get('unity'),
                    field_name: 'unity',
                    id_key: 'value'
                }
            )
        );
        this.showChildView(
            'tva',
            new SelectWidget(
                {
                    options: this.tva_options,
                    title: "TVA",
                    value: this.model.get('tva'),
                    field_name: 'tva',
                    id_key: 'value'
                }
            )
        );
        this.product_options = this.getProductOptions(
            this.tva_options,
            this.all_product_options,
        );
        console.log(this.model);
        console.log(this.product_options);
        console.log(this.all_product_options);
        console.log(this.tva_options);
        this.showChildView(
            'product_id',
            new SelectWidget(
                {
                    options: this.product_options,
                    title: "Compte produit",
                    value: this.model.get('product_id'),
                    field_name: 'product_id',
                    id_key: 'id'
                }
            )
        );
        if (this.isAddView()){
            this.getUI('main_tab').tab('show');
        }
    },
    getProductOptions(tva_options, product_options){
       /*
        *  Return an array on filtered products option depending on selected tva
        * NB : Here we override the mixins method since the TaskLines uses tva values :/
        *  :params list tva_options:
        *  :params list product_options:
        */
        let tva_id = this.getDefaultTva()['id'];
        if (this.model.has('tva')){
            let tva_value = this.model.get('tva');
            let tva_object =_.findWhere(this.tva_options, {value: parseFloat(tva_value)});
            if (tva_object){
                tva_id = tva_object.id;
            }
        }
        let options = [];

        if(tva_id) {
            options = _.filter(product_options, function(product) {
              return product.tva_id === parseInt(tva_id);
            });
        }
        return options;
    },
    onRender: function(){
        this.refreshForm();
        if (this.isAddView()){
            this.showChildView(
                'catalogContainer',
                new CatalogComponent(
                    {
                        'params': {type_: 'product'},
                        'collection_name': 'catalog_tree',
                    }
                )
            );
        }
    }
});
export default TaskLineFormView;
