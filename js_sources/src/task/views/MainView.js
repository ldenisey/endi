import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import FileBlockView from "./FileBlockView.js";
import GeneralView from "./GeneralView.js";
import CommonView from "./CommonView.js";
import CompositionComponent from './CompositionComponent.js';
import NotesBlockView from './NotesBlockView.js';
import PaymentConditionBlockView from './payments/PaymentConditionBlockView.js';
import PaymentBlockView from './payments/PaymentBlockView.js';
import ActionView from "./ActionView.js";
import StatusView from './StatusView.js';
import LoginView from '../../base/views/LoginView.js';
import ErrorView from '../../base/views/ErrorView.js';
import { showLoader, hideLoader } from '../../tools.js';

const template = require('./templates/MainView.mustache');

const MainView = Mn.View.extend({
    template: template,
    regions: {
        errors: '.errors',
        modalRegion: '#modalregion',
        file_requirements: "#file_requirements",
        general: '.general-informations',
        common: '.common-informations',
        composition: '.composition',
        actions_toolbar: "#actions_toolbar",
        ht_before_discounts: '.ht_before_discounts',
        notes: '.notes',
        payment_conditions: '.payment-conditions',
        payments: '.payments'
    },
    childViewEvents: {
        'status:change': 'onStatusChange',
    },
    initialize: function(options){
        this.config = Radio.channel('config');
        this.facade = Radio.channel('facade');
    },
    showFileBlock: function(){
        var section = this.config.request(
            'get:form_section', 'file_requirements'
        );
        var collection = this.facade.request(
            'get:filerequirementcollection'
        );
        var view = new FileBlockView(
            {collection: collection, section:section}
        );
        this.showChildView('file_requirements', view);
    },
    showGeneralBlock: function(){
        var section = this.config.request('get:form_section', 'general');
        var model = this.facade.request('get:model', 'general');
        var view = new GeneralView({model: model, section: section});
        this.showChildView('general', view);
    },
    showCommonBlock: function(){
        var section = this.config.request('get:form_section', 'common');
        var model = this.facade.request('get:model', 'common');
        var view = new CommonView({model: model, section: section});
        this.showChildView('common', view);
    },
    showNotesBlock: function(){
        var section = this.config.request('get:form_section', 'notes');
        var model = this.facade.request('get:model', 'notes');
        var view = new NotesBlockView({model: model, section: section});
        this.showChildView('notes', view);
    },
    showPaymentConditionsBlock: function(){
        var section = this.config.request('get:form_section', 'payment_conditions');
        var model = this.facade.request('get:model', 'payment_conditions');
        var view = new PaymentConditionBlockView({model:model});
        this.showChildView('payment_conditions', view);
    },
    showPaymentBlock: function(){
        var section = this.config.request('get:form_section', 'payments');
        var model = this.facade.request('get:model', 'common');
        var collection = this.facade.request('get:collection', 'payment_lines');
        var view = new PaymentBlockView(
            {model: model, collection: collection, section: section}
        );
        this.showChildView('payments', view);
    },
    showLogin: function(){
        var view = new LoginView({});
        this.showChildView('modalRegion', view);
    },
    onRender: function() {
        var totalmodel = this.facade.request('get:totalmodel');
        var view;
        if (this.config.request('has:form_section', 'file_requirements')){
            this.showFileBlock();
        }
        if (this.config.request('has:form_section', 'general')){
            this.showGeneralBlock();
        }
        if (this.config.request('has:form_section', 'common')){
            this.showCommonBlock();
        }
        if (this.config.request('has:form_section', 'composition')){
            this.showCompositionBlock(totalmodel);
        }
        if (this.config.request('has:form_section', "notes")){
            this.showNotesBlock();
        }
        if (this.config.request('has:form_section', "payment_conditions")){
            this.showPaymentConditionsBlock();
        }
        if (this.config.request('has:form_section', "payments")){
            this.showPaymentBlock();
        }
        view = new ActionView({
            actions: this.config.request('get:form_actions'),
            model: totalmodel
        });
        this.showChildView('actions_toolbar', view);
    },
    showCompositionBlock(totalmodel){
        let section = this.config.request('get:form_section', 'composition');
        let view = new CompositionComponent(
            {section:section, totalmodel: totalmodel}
        );
        this.showChildView('composition', view);
    },
    showStatusView(status, title, label, url){
        var model = this.facade.request('get:model', 'common');
        var view = new StatusView({
            status: status,
            title: title,
            label: label,
            model: model,
            url: url
        });
        this.showChildView('modalRegion', view);
    },
    formOk(){
        var result = true;
        var errors = this.facade.request('is:valid');
        if (!_.isEmpty(errors)){
            this.showChildView(
                'errors',
                new ErrorView({errors:errors})
            );
            result = false;
        } else {
            this.detachChildView('errors');
        }
        return result;
    },
    onStatusChange: function(status, title, label, url){
        if (! status){
            return;
        }
        showLoader();
        if (status != 'draft'){
            if (! this.formOk()){
                document.body.scrollTop = document.documentElement.scrollTop = 0;
                hideLoader();
                return;
            }
        }
        var this_ = this;
        let deferred = this.facade.request('save:all');
        deferred.then(
            function(){
                console.log("In the then");
                    hideLoader();
                    this_.showStatusView(status, title, label, url)
            },
            function(){
                console.log("In the then error method");
            }
        );
    },
    onChildviewDestroyModal: function() {
    	this.getRegion('modalRegion').empty();
  	}
});
export default MainView;
